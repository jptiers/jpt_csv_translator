**Verision 1.0**

-Parseos de la data para pruebas.

-Verificacion de la entrada de datos.

-Validacion de ruta y extension.

**Version 1.1**

-Correcion de enrutamiento para almacenamiento de los archivos .csv.

-Adicion del elemento que contiene los check para seleccionar sensores.

-Se adiciono una validacion para el boton del parseo.

-Se implemento una clase que permite la lectura de la base de datos.

**Version 1.2**

-El boton originalmente que tenia la funcion de parsear, se reemplazo para validar.

-Se adiciono otro para procesar los datos de la base de datos despues de validada.

-Se agrego una clase la cual ayudaria como modelo para la recepcion de datos de los diferentes nodos.

-Se habilito la parte del check para seleccionar que nodos se desean parsear.

-Se adiciono un rango de fechas para el parseo de los datos.

**Version 1.3**

-Anteriomente el parseo de los datos era solor de tres nodos (pluviometro, umbral, nivel), ahora lee otro mas que es distanciometro.

-Los mensajes de la consola ahora se generan por medio de señales en el proceso del software.