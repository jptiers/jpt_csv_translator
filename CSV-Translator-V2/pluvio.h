#ifndef PLUVIO_H
#define PLUVIO_H

#include <QObject>

class pluvio
{
public:
     bool status=false;
    pluvio();

    void setID(int id);
    int getID();

    //setters anf getters id_device
    void set_id_device(QString my_id_device);
    QString get_id_device();
    //setters and getters time_stamp
    void add_item_time(QString add_time);
    QStringList get_list_time_stamp();
    QString get_item_list_time_stamp(int position);
    //*********************************************************************
    //setters and getters precipitation
    void add_item_precipitation(QString precipitation);
    QStringList get_list_precipitation();
    QString get_item_list_precipitation(int position);

    void add_item_raw_precipitation(QString precipitation);
    QStringList get_list_raw_precipitation();
    QString get_item_list_raw_precipitation(int position);

    //*********************************************************************
    //setters and getters acumulada
    void add_item_acumulada(QString acumulada);
    QStringList get_list_acumulada();
    QString get_item_list_acumulada(int position);

    void add_item_raw_acumulada(QString acumulada);
    QStringList get_list_raw_acumulada();
    QString get_item_list_raw_acumulada(int position);
    //*********************************************************************

    //*********************************************************************
    //setters and getters humidity
    void add_item_humidity(QString humidity);
    QStringList get_list_humidity();
    QString get_item_list_humidity(int position);

    void add_item_raw_humidity(QString humidity);
    QStringList get_list_raw_humidity();
    QString get_item_list_raw_humidity(int position);
    //*********************************************************************

    //*********************************************************************
    //setters and getters temperature
    void add_item_temperature(QString temperature);
    QStringList get_list_temperature();
    QString get_item_list_temperature(int position);

    void add_item_raw_temperature(QString temperature);
    QStringList get_list_raw_temperature();
    QString get_item_list_raw_temperature(int position);
    //*********************************************************************

    //*********************************************************************
    //setters and getters presure
    void add_item_presure(QString presure);
    QStringList get_list_presure();
    QString get_item_list_presure(int position);

    void add_item_raw_presure(QString presure);
    QStringList get_list_raw_presure();
    QString get_item_list_raw_presure(int position);
    //*********************************************************************


    int count_elements();

private:
    int ID;
    QString id_device;
    QStringList list_time_stamp;
    QStringList list_precipitation;
    QStringList list_acumulada;
    QStringList list_humidity;
    QStringList list_temperature;
    QStringList list_presure;

    QStringList list_Raw_precipitation;
    QStringList list_Raw_acumulada;
    QStringList list_Raw_humidity;
    QStringList list_Raw_temperature;
    QStringList list_Raw_presure;
};

#endif // PLUVIO_H
