#ifndef FERMENTER_H
#define FERMENTER_H

#include <QObject>

class fermenter
{
public:
    fermenter();
    bool status=false;

    void setID(int id);
    int getID();

    //setters anf getters id_device
    void set_id_device(QString my_id_device);
    QString get_id_device();
    //setters and getters time_stamp
    void add_item_time(QString add_time);
    QStringList get_list_time_stamp();
    QString get_item_list_time_stamp(int position);
    //*********************************************************************
    //setters and getters ph
    void add_item_ph(QString ph);
    QStringList get_list_ph();
    QString get_item_list_ph(int position);

    void add_item_raw_ph(QString ph);
    QStringList get_list_raw_ph();
    QString get_item_list_raw_ph(int position);

    //*********************************************************************
    //setters and getters resistivity1
    void add_item_resistivity1(QString resistivity1);
    QStringList get_list_resistivity1();
    QString get_item_list_resistivity1(int position);

    void add_item_raw_resistivity1(QString resistivity1);
    QStringList get_list_raw_resistivity1();
    QString get_item_list_raw_resistivity1(int position);
    //*********************************************************************

    //*********************************************************************
    //setters and getters resistivity2
    void add_item_resistivity2(QString resistivity2);
    QStringList get_list_resistivity2();
    QString get_item_list_resistivity2(int position);

    void add_item_raw_resistivity2(QString resistivity2);
    QStringList get_list_raw_resistivity2();
    QString get_item_list_raw_resistivity2(int position);
    //*********************************************************************

    //*********************************************************************
    //setters and getters temperatura
    void add_item_temperature1(QString temperature1);
    QStringList get_list_temperature1();
    QString get_item_list_temperature1(int position);

    void add_item_raw_temperature1(QString temperature1);
    QStringList get_list_raw_temperature1();
    QString get_item_list_raw_temperature1(int position);
    //*********************************************************************

    //*********************************************************************
    //setters and getters temperatura 2
    void add_item_temperature2(QString temperature2);
    QStringList get_list_temperature2();
    QString get_item_list_temperature2(int position);

    void add_item_raw_temperature2(QString temperature2);
    QStringList get_list_raw_temperature2();
    QString get_item_list_raw_temperature2(int position);
    //*********************************************************************

    //*********************************************************************
    //setters and getters temperatura 3
    void add_item_temperature3(QString temperature3);
    QStringList get_list_temperature3();
    QString get_item_list_temperature3(int position);

    void add_item_raw_temperature3(QString temperature3);
    QStringList get_list_raw_temperature3();
    QString get_item_list_raw_temperature3(int position);
    //*********************************************************************

    //*********************************************************************
    //setters and getters temperatura 4
    void add_item_temperature4(QString temperature4);
    QStringList get_list_temperature4();
    QString get_item_list_temperature4(int position);

    void add_item_raw_temperature4(QString temperature4);
    QStringList get_list_raw_temperature4();
    QString get_item_list_raw_temperature4(int position);
    //*********************************************************************


    //*********************************************************************
    //setters and getters alcohol
    void add_item_alcohol(QString alcohol);
    QStringList get_list_alcohol();
    QString get_item_list_alcohol(int position);

    void add_item_raw_alcohol(QString alcohol);
    QStringList get_list_raw_alcohol();
    QString get_item_list_raw_alcohol(int position);
    //*********************************************************************


    //*********************************************************************
    //setters and getters methano
    void add_item_methano(QString methano);
    QStringList get_list_methano();
    QString get_item_list_methano(int position);

    void add_item_raw_methano(QString methano);
    QStringList get_list_raw_methano();
    QString get_item_list_raw_methano(int position);
    //*********************************************************************

    int count_elements();

private:
    int ID;
    QString id_device;
    QStringList list_time_stamp;
    QStringList list_ph;
    QStringList list_resistivity1;
    QStringList list_resistivity2;
    QStringList list_temperatura1;
    QStringList list_temperatura2;
    QStringList list_temperatura3;
    QStringList list_temperatura4;
    QStringList list_alcohol;
    QStringList list_methano;

    QStringList list_Raw_ph;
    QStringList list_Raw_resistivity1;
    QStringList list_Raw_resistivity2;
    QStringList list_Raw_temperatura1;
    QStringList list_Raw_temperatura2;
    QStringList list_Raw_temperatura3;
    QStringList list_Raw_temperatura4;
    QStringList list_Raw_alcohol;
    QStringList list_Raw_methano;

};

#endif // FERMENTER_H
