#ifndef TRANSLATOR_H
#define TRANSLATOR_H
#include "class/jptdatabase.h"
#include "umbral.h"
#include "nivel.h"
#include "pluvio.h"
#include "distanciometer.h"
#include "fermenter.h"
#include <QMainWindow>
#include <QDate>

namespace Ui {
class translator;
}

class translator : public QMainWindow
{
    Q_OBJECT

public:
    QString getDateStart();
    QString getDateEnd();
    void dateStart(QString);
    void dateEnd(QString);

    explicit translator(QWidget *parent = nullptr);
    ~translator();

signals:
    void msgConsole(QString msg);

public slots:
     void select_db();
     void test_db();
     void inspecct_db();
     void parse_data();

     void sendMsgConsole(QString msg);

     void change_estate_dir(const QString data);
     void dateStartSlot(QDate);
     void dateEndSlot(QDate);

private:
    jptdatabase *db;

    QVector<umbral> list_umbrales;
    QVector<nivel> list_niveles;
    QVector<pluvio> list_pluvio;
    QVector<distanciometer> list_distan;
    QVector<fermenter> list_fermenter;

    QString date1;
    QString date2;
    Ui::translator *ui;
};

#endif // TRANSLATOR_H
