#include "nivel.h"

nivel::nivel()
{

}
void nivel::setID(int id){
    ID = id;
}

int nivel::getID(){
    return ID;
}

//setters anf getters id_device
void nivel::set_id_device(QString my_id_device)
{
    id_device=my_id_device;
}

QString nivel::get_id_device()
{
    return id_device;

}

//setters and getters time_stamp
void nivel::add_item_time(QString add_time)
{
    list_time_stamp.append(add_time);
}
QStringList nivel::get_list_time_stamp()
{
    return list_time_stamp;
}
QString nivel::get_item_list_time_stamp(int position)
{
    return list_time_stamp.value(position);
}


//*****************************************************************************
//setters and getters nivel
void nivel::add_item_nivel(QString nivel)
{
    list_nivel.append(nivel);
}
QStringList nivel::get_list_nivel()
{
    return list_nivel;
}
QString nivel::get_item_list_nivel(int position)
{
    return list_nivel.value(position);

}


void nivel::add_item_raw_nivel(QString nivel)
{
    list_Raw_nivel.append(nivel);
}
QStringList nivel::get_list_raw_nivel()
{
    return list_Raw_nivel;
}
QString nivel::get_item_list_raw_nivel(int position)
{
    return list_Raw_nivel.value(position);

}

//*****************************************************************************
//*****************************************************************************
//setters and getters temperature
void nivel::add_item_temperature(QString temperature)
{
    list_temperatura.append(temperature);
}
QStringList nivel::get_list_temperature()
{
    return list_temperatura;
}
QString nivel::get_item_list_temperature(int position)
{
    return list_temperatura.value(position);

}


void nivel::add_item_raw_temperature(QString temperature)
{
    list_Raw_temperatura.append(temperature);
}
QStringList nivel::get_list_raw_temperature()
{
    return list_Raw_temperatura;
}
QString nivel::get_item_list_raw_temperature(int position)
{
    return list_Raw_temperatura.value(position);

}

//*******************************************

//*****************************************************************************
//*****************************************************************************
//setters and getters temperature
void nivel::add_item_vibration(QString vibration)
{
    list_vibration.append(vibration);
}
QStringList nivel::get_list_vibration()
{
    return list_vibration;
}
QString nivel::get_item_list_vibration(int position)
{
    return list_vibration.value(position);

}


void nivel::add_item_raw_vibration(QString vibration)
{
    list_Raw_vibration.append(vibration);
}
QStringList nivel::get_list_raw_vibration()
{
    return list_Raw_vibration;
}
QString nivel::get_item_list_raw_vibration(int position)
{
    return list_Raw_vibration.value(position);

}

//*******************************************

//*****************************************************************************
//*****************************************************************************
//setters and getters temperature
void nivel::add_item_sound(QString sound)
{
    list_sound.append(sound);
}
QStringList nivel::get_list_sound()
{
    return list_sound;
}
QString nivel::get_item_list_sound(int position)
{
    return list_sound.value(position);

}


void nivel::add_item_raw_sound(QString sound)
{
    list_Raw_sound.append(sound);
}
QStringList nivel::get_list_raw_sound()
{
    return list_Raw_sound;
}
QString nivel::get_item_list_raw_sound(int sound)
{
    return list_Raw_sound.value(sound);

}

//*******************************************

int nivel::count_elements()
{
    return list_Raw_nivel.count();//se devuelve la cuanta de cualquier variable.

}



