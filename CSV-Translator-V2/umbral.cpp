#include "umbral.h"

umbral::umbral()
{

}

void umbral::setID(int id){
    ID = id;
}

int umbral::getID(){
    return ID;
}


//setters anf getters id_device
void umbral::set_id_device(QString my_id_device)
{
    id_device=my_id_device;
}


QString umbral::get_id_device()
{
    return id_device;
}

//setters and getters time_stamp
void umbral::add_item_time(QString add_time)
{
    list_time_stamp.append(add_time);
}
QStringList umbral::get_list_time_stamp()
{
    return list_time_stamp;
}
QString umbral::get_item_list_time_stamp(int position)
{
    return list_time_stamp.value(position);
}


//*****************************************************************************
//setters and getters acc_x
void umbral::add_item_acc_x(QString acc_x)
{
    list_acc_x.append(acc_x);
}
QStringList umbral::get_list_acc_x()
{
    return list_acc_x;
}
QString umbral::get_item_list_acc_x(int position)
{
    return list_acc_x.value(position);

}


void umbral::add_item_raw_acc_x(QString acc_x)
{
    list_Raw_acc_x.append(acc_x);
}
QStringList umbral::get_list_raw_acc_x()
{
    return list_Raw_acc_x;
}
QString umbral::get_item_list_raw_acc_x(int position)
{
    return list_Raw_acc_x.value(position);

}

//*****************************************************************************




//setters and getters acc_y
void umbral::add_item_acc_y(QString acc_y)
{
    list_acc_y.append(acc_y);
}
QStringList umbral::get_list_acc_y()
{
    return list_acc_y;
}
QString umbral::get_item_list_acc_y(int position)
{
    return list_acc_y.value(position);

}


void umbral::add_item_raw_acc_y(QString acc_y)
{
    list_Raw_acc_y.append(acc_y);
}
QStringList umbral::get_list_raw_acc_y()
{
    return list_Raw_acc_y;
}
QString umbral::get_item_list_raw_acc_y(int position)
{
    return list_Raw_acc_y.value(position);

}


//*************************************************************

//setters and getters acc_z
void umbral::add_item_acc_z(QString acc_z)
{
    list_acc_z.append(acc_z);
}
QStringList umbral::get_list_acc_z()
{
    return list_acc_z;
}
QString umbral::get_item_list_acc_z(int position)
{
    return list_acc_z.value(position);

}



void umbral::add_item_raw_acc_z(QString acc_z)
{
    list_Raw_acc_z.append(acc_z);
}
QStringList umbral::get_list_raw_acc_z()
{
    return list_Raw_acc_z;
}
QString umbral::get_item_list_raw_acc_z(int position)
{
    return list_Raw_acc_z.value(position);

}


//***********************************************************

//setters and getters vib_x
void umbral::add_item_vib_x(QString vib_x)
{
    list_vib_x.append(vib_x);
}
QStringList umbral::get_list_vib_x()
{
    return list_vib_x;
}
QString umbral::get_item_list_vib_x(int position)
{
    return list_vib_x.value(position);

}


void umbral::add_item_raw_vib_x(QString vib_x)
{
    list_Raw_vib_x.append(vib_x);
}
QStringList umbral::get_list_raw_vib_x()
{
    return list_Raw_vib_x;
}
QString umbral::get_item_list_raw_vib_x(int position)
{
    return list_Raw_vib_x.value(position);

}


//**************************************************************
//setters and getters vib_y
void umbral::add_item_vib_y(QString vib_y)
{
    list_vib_y.append(vib_y);
}
QStringList umbral::get_list_vib_y()
{
    return list_vib_y;
}
QString umbral::get_item_list_vib_y(int position)
{
    return list_vib_y.value(position);

}



void umbral::add_item_raw_vib_y(QString vib_y)
{
    list_Raw_vib_y.append(vib_y);
}
QStringList umbral::get_list_raw_vib_y()
{
    return list_Raw_vib_y;
}
QString umbral::get_item_list_raw_vib_y(int position)
{
    return list_Raw_vib_y.value(position);

}




//****************************************************************+
//setters and getters vib_z
void umbral::add_item_vib_z(QString vib_z)
{
    list_vib_z.append(vib_z);
}
QStringList umbral::get_list_vib_z()
{
    return list_vib_z;
}
QString umbral::get_item_list_vib_z(int position)
{
    return list_vib_z.value(position);

}

void umbral::add_item_raw_vib_z(QString vib_z)
{
    list_Raw_vib_z.append(vib_z);
}
QStringList umbral::get_list_raw_vib_z()
{
    return list_Raw_vib_z;
}
QString umbral::get_item_list_raw_vib_z(int position)
{
    return list_Raw_vib_z.value(position);
}



//********************************************************************
//setters y getter de humedad
void umbral::add_item_hum(QString humedad)
{
    list_hum.append(humedad);
}
QStringList umbral::get_list_hum()
{
    return list_hum;
}
QString umbral::get_item_list_hum(int position)
{
    return list_hum.value(position);
}

void umbral::add_item_raw_hum(QString humedad)
{
    list_Raw_hum.append(humedad);
}
QStringList umbral::get_list_raw_hum()
{
    return list_Raw_hum;
}
QString umbral::get_item_list_raw_hum(int position)
{
    return list_Raw_hum.value(position);
}
//*******************************************

int umbral::count_elements()
{
    return list_Raw_acc_x.count();//se devuelve la cuanta de cualquier variable.

}
