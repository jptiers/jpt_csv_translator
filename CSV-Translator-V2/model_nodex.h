#ifndef MODEL_NODEX_H
#define MODEL_NODEX_H

#include <QObject>

class model_nodex
{
private:
    int ID;
    QString m_datetime;
    QString m_data;
    QString m_device;
public:
   model_nodex();

   void setID(int id);
   int getID();

   void setDateTime(QString dateTime);
   QString getDateTime();

   void setData(QString data);
   QString getData();

   void setDevice(QString device);
   QString getDevice();

   int getCount();

   void init_model(int id,QString data,QString device,QString time);

};

#endif // MODEL_DB_H
