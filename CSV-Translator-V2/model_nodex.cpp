#include "model_nodex.h"

model_nodex::model_nodex()
{

}

void model_nodex::setID(int id){
    ID = id;
}

int model_nodex::getID(){
    return ID;
}

void model_nodex::setDateTime(QString dateTime){
    m_datetime = dateTime;
}

QString model_nodex::getDateTime(){
    return m_datetime;
}

void model_nodex::setData(QString data){
    m_data = data;
}

QString model_nodex::getData(){
    return m_data;
}

void model_nodex::setDevice(QString device){
    m_device = device;
}

void model_nodex::init_model(int id,QString data,QString device,QString time)
{
   this->setID(id);
   this->setDateTime(time);
   this->setData(data);
   this->setDevice(device);
}


