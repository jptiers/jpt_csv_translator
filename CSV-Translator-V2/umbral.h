#ifndef UMBRAL_H
#define UMBRAL_H

#include <QObject>

class umbral
{
public:
    bool status=false;
    umbral();

    void setID(int id);
    int getID();

    //setters anf getters id_device
    void set_id_device(QString my_id_device);
    QString get_id_device();
    //setters and getters time_stamp
    void add_item_time(QString add_time);
    QStringList get_list_time_stamp();
    QString get_item_list_time_stamp(int position);
    //*********************************************************************
    //setters and getters acc_x
    void add_item_acc_x(QString acc_x);
    QStringList get_list_acc_x();
    QString get_item_list_acc_x(int position);

    void add_item_raw_acc_x(QString acc_x);
    QStringList get_list_raw_acc_x();
    QString get_item_list_raw_acc_x(int position);

    //*********************************************************************
    //setters and getters acc_y
    void add_item_acc_y(QString acc_y);
    QStringList get_list_acc_y();
    QString get_item_list_acc_y(int position);

    void add_item_raw_acc_y(QString acc_y);
    QStringList get_list_raw_acc_y();
    QString get_item_list_raw_acc_y(int position);
    //*********************************************************************
    //setters and getters acc_z
    void add_item_acc_z(QString acc_z);
    QStringList get_list_acc_z();
    QString get_item_list_acc_z(int position);

    void add_item_raw_acc_z(QString acc_z);
    QStringList get_list_raw_acc_z();
    QString get_item_list_raw_acc_z(int position);
    //*********************************************************************
    //setters and getters vib_x
    void add_item_vib_x(QString vib_x);
    QStringList get_list_vib_x();
    QString get_item_list_vib_x(int position);


    void add_item_raw_vib_x(QString vib_x);
    QStringList get_list_raw_vib_x();
    QString get_item_list_raw_vib_x(int position);
    //*********************************************************************
    //setters and getters vib_y
    void add_item_vib_y(QString vib_y);
    QStringList get_list_vib_y();
    QString get_item_list_vib_y(int position);

    void add_item_raw_vib_y(QString vib_y);
    QStringList get_list_raw_vib_y();
    QString get_item_list_raw_vib_y(int position);
    //*********************************************************************
    //setters and getters vib_z
    void add_item_vib_z(QString vib_z);
    QStringList get_list_vib_z();
    QString get_item_list_vib_z(int position);

    void add_item_raw_vib_z(QString vib_z);
    QStringList get_list_raw_vib_z();
    QString get_item_list_raw_vib_z(int position);

    //*********************************
    //setters y getter de humedad
    void add_item_hum(QString humedad);
    QStringList get_list_hum();
    QString get_item_list_hum(int position);

    void add_item_raw_hum(QString humedad);
    QStringList get_list_raw_hum();
    QString get_item_list_raw_hum(int position);

    int count_elements();

private:

    //Cada sensor maneja siempre dos variables
    //RAW  e INGENIERA
    //RAW: este valor sn los datos cruos del sensor.
    //ING: este valor son los datos en funcion del entendimieto humano
    //ejemplo: 15683 unidades son cuentas.  = 43C.
    //ejemplo:  sensor nivel de rio. 560 cuentas= 130cm. Sensor de caudal que calcula , caudal del rio com la integral del nivel del rio.



    //*******************************************************************************************
    //***************************************Esquematico*****************************************
    //*******************************************************************************************

    /* El sistema consta de 4 umbrales, cada umbral tiene 7 sensores
     * Umbral 1. (Vector)=>list_umbrales[0]
     * cada sensor que tiee maneja informacon respecto del tiempo:
     * por eso se almacena en listas dentro del vector
     * Umbral 1. (Vector)=>list_umbrales[0]
     *      =>valor tiempo 1--> tiempo[0] -->acc_y[0]
     *      =>valor tiempo 2-->tiempo [1] -->-->acc_y[0]
     *      =>valor tiempo 3-->tiempo [2]
     * //son listas por la cantidad de datos.
     *
     *
     *
     *
     */
    int ID;
    QString id_device;
    QStringList list_time_stamp;
    QStringList list_acc_x;
    QStringList list_acc_y;
    QStringList list_acc_z;
    QStringList list_hum;
    QStringList list_vib_x;
    QStringList list_vib_y;
    QStringList list_vib_z;

    QStringList list_Raw_acc_x;
    QStringList list_Raw_acc_y;
    QStringList list_Raw_acc_z;
    QStringList list_Raw_hum;
    QStringList list_Raw_vib_x;
    QStringList list_Raw_vib_y;
    QStringList list_Raw_vib_z;
};

#endif // UMBRAL_H
