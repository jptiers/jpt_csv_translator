#include "pluvio.h"

pluvio::pluvio()
{

}
//SET/GET ID DE LA BD
void pluvio::setID(int id){
    ID = id;
}

int pluvio::getID(){
    return ID;
}

//setters anf getters id_device
void pluvio::set_id_device(QString my_id_device)
{
    id_device=my_id_device;
}

QString pluvio::get_id_device()
{
    return id_device;

}

//setters and getters time_stamp
void pluvio::add_item_time(QString add_time)
{
    list_time_stamp.append(add_time);
}
QStringList pluvio::get_list_time_stamp()
{
    return list_time_stamp;
}
QString pluvio::get_item_list_time_stamp(int position)
{
    return list_time_stamp.value(position);
}


//*****************************************************************************
//setters and getters  precipitation
void pluvio::add_item_precipitation(QString precipitation)
{
    list_precipitation.append(precipitation);
}
QStringList pluvio::get_list_precipitation()
{
    return list_precipitation;
}
QString pluvio::get_item_list_precipitation(int position)
{
    return list_precipitation.value(position);

}


void pluvio::add_item_raw_precipitation(QString precipitation)
{
    list_Raw_precipitation.append(precipitation);
}
QStringList pluvio::get_list_raw_precipitation()
{
    return list_Raw_precipitation;
}
QString pluvio::get_item_list_raw_precipitation(int position)
{
    return list_Raw_precipitation.value(position);

}

//*****************************************************************************
//*****************************************************************************
//setters and getters acumulada
void pluvio::add_item_acumulada(QString acumulada)
{
    list_acumulada.append(acumulada);
}
QStringList pluvio::get_list_acumulada()
{
    return list_acumulada;
}
QString pluvio::get_item_list_acumulada(int position)
{
    return list_acumulada.value(position);

}


void pluvio::add_item_raw_acumulada(QString acumulada)
{
    list_Raw_acumulada.append(acumulada);
}
QStringList pluvio::get_list_raw_acumulada()
{
    return list_Raw_acumulada;
}
QString pluvio::get_item_list_raw_acumulada(int position)
{
    return list_Raw_acumulada.value(position);

}

//*******************************************

//*****************************************************************************
//*****************************************************************************
//setters and getters acumulada
void pluvio::add_item_humidity(QString humidity)
{
    list_humidity.append(humidity);
}
QStringList pluvio::get_list_humidity()
{
    return list_humidity;
}
QString pluvio::get_item_list_humidity(int position)
{
    return list_humidity.value(position);
}


void pluvio::add_item_raw_humidity(QString humidity)
{
    list_Raw_humidity.append(humidity);
}
QStringList pluvio::get_list_raw_humidity()
{
    return list_Raw_humidity;
}
QString pluvio::get_item_list_raw_humidity(int position)
{
    return list_Raw_humidity.value(position);

}

//*******************************************

//*****************************************************************************
//*****************************************************************************
//setters and getters temperature
void pluvio::add_item_temperature(QString temperature)
{
    list_temperature.append(temperature);
}
QStringList pluvio::get_list_temperature()
{
    return list_temperature;
}
QString pluvio::get_item_list_temperature(int position)
{
    return list_temperature.value(position);
}


void pluvio::add_item_raw_temperature(QString temperature)
{
    list_Raw_temperature.append(temperature);
}
QStringList pluvio::get_list_raw_temperature()
{
    return list_Raw_temperature;
}
QString pluvio::get_item_list_raw_temperature(int position)
{
    return list_Raw_temperature.value(position);

}

//*******************************************

//*****************************************************************************
//*****************************************************************************
//setters and getters presure
void pluvio::add_item_presure(QString presure)
{
    list_presure.append(presure);
}
QStringList pluvio::get_list_presure()
{
    return list_presure;
}
QString pluvio::get_item_list_presure(int position)
{
    return list_presure.value(position);
}


void pluvio::add_item_raw_presure(QString presure)
{
    list_Raw_presure.append(presure);
}
QStringList pluvio::get_list_raw_presure()
{
    return list_Raw_presure;
}
QString pluvio::get_item_list_raw_presure(int position)
{
    return list_Raw_presure.value(position);

}

//*******************************************


int pluvio::count_elements()
{
    return list_Raw_precipitation.count();//se devuelve la cuanta de cualquier variable.

}

