#include "distanciometer.h"

distanciometer::distanciometer(){}

void distanciometer::setID(int id){
    ID = id;
}

int distanciometer::getID(){
    return ID;
}

//setters anf getters id_device
void distanciometer::set_id_device(QString my_id_device){
    id_device=my_id_device;
}

QString distanciometer::get_id_device(){
    return id_device;
}
//setters and getters time_stamp
void distanciometer::add_item_time(QString add_time){
    list_time_stamp.append(add_time);
}
QStringList distanciometer::get_list_time_stamp(){
    return list_time_stamp;
}
QString distanciometer::get_item_list_time_stamp(int position){
    return list_time_stamp.value(position);
}

//*****************************************************************************
//setters and getters distanciometer
void distanciometer::add_item_distanciometer(QString distanciometer){
    list_distanciometer.append(distanciometer);
}

QStringList distanciometer::get_list_distanciometer(){
    return list_distanciometer;
}

QString distanciometer::get_item_list_distanciometer(int position){
    return list_distanciometer.value(position);
}

void distanciometer::add_item_raw_distanciometer(QString distanciometer){
    list_Raw_distanciometer.append(distanciometer);
}

QStringList distanciometer::get_list_raw_distanciometer(){
    return list_Raw_distanciometer;
}

QString distanciometer::get_item_list_raw_distanciometer(int position){
    return list_Raw_distanciometer.value(position);
}

int distanciometer::count_elements()
{
    return list_Raw_distanciometer.count();//se devuelve la cuanta de cualquier variable.

}
