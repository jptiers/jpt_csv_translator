#include "fermenter.h"

fermenter::fermenter()
{

}

void fermenter::setID(int id){
    ID = id;
}

int fermenter::getID(){
    return ID;
}

//setters anf getters id_device
void fermenter::set_id_device(QString my_id_device)
{
    id_device=my_id_device;
}

QString fermenter::get_id_device()
{
    return id_device;

}

//setters and getters time_stamp
void fermenter::add_item_time(QString add_time)
{
    list_time_stamp.append(add_time);
}
QStringList fermenter::get_list_time_stamp()
{
    return list_time_stamp;
}
QString fermenter::get_item_list_time_stamp(int position)
{
    return list_time_stamp.value(position);
}


//*****************************************************************************
//setters and getters nivel
void fermenter::add_item_ph(QString ph)
{
    list_ph.append(ph);
}
QStringList fermenter::get_list_ph()
{
    return list_ph;
}
QString fermenter::get_item_list_ph(int position)
{
    return list_ph.value(position);

}


void fermenter::add_item_raw_ph(QString ph)
{
    list_Raw_ph.append(ph);
}
QStringList fermenter::get_list_raw_ph()
{
    return list_Raw_ph;
}
QString fermenter::get_item_list_raw_ph(int position)
{
    return list_Raw_ph.value(position);

}

//*****************************************************************************
//*****************************************************************************
//setters and getters temperature
void fermenter::add_item_resistivity1(QString resistivity1)
{
    list_resistivity1.append(resistivity1);
}
QStringList fermenter::get_list_resistivity1()
{
    return list_resistivity1;
}
QString fermenter::get_item_list_resistivity1(int position)
{
    return list_resistivity1.value(position);

}

void fermenter::add_item_raw_resistivity1(QString resistivity1)
{
    list_Raw_resistivity1.append(resistivity1);
}
QStringList fermenter::get_list_raw_resistivity1()
{
    return list_Raw_resistivity1;
}
QString fermenter::get_item_list_raw_resistivity1(int position)
{
    return list_Raw_resistivity1.value(position);

}

void fermenter::add_item_resistivity2(QString resistivity2)
{
    list_resistivity2.append(resistivity2);
}
QStringList fermenter::get_list_resistivity2()
{
    return list_resistivity2;
}
QString fermenter::get_item_list_resistivity2(int position)
{
    return list_resistivity2.value(position);

}


void fermenter::add_item_raw_resistivity2(QString resistivity2)
{
    list_Raw_resistivity2.append(resistivity2);
}
QStringList fermenter::get_list_raw_resistivity2()
{
    return list_Raw_resistivity2;
}
QString fermenter::get_item_list_raw_resistivity2(int position)
{
    return list_Raw_resistivity2.value(position);

}

//*******************************************

//*****************************************************************************
//*****************************************************************************
//setters and getters temperature
void fermenter::add_item_temperature1(QString temperature1)
{
    list_temperatura1.append(temperature1);
}
QStringList fermenter::get_list_temperature1()
{
    return list_temperatura1;
}
QString fermenter::get_item_list_temperature1(int position)
{
    return list_temperatura1.value(position);

}


void fermenter::add_item_raw_temperature1(QString temperature1)
{
    list_Raw_temperatura1.append(temperature1);
}
QStringList fermenter::get_list_raw_temperature1()
{
    return list_Raw_temperatura1;
}
QString fermenter::get_item_list_raw_temperature1(int position)
{
    return list_Raw_temperatura1.value(position);

}

//*******************************************

//*****************************************************************************
//*****************************************************************************
//setters and getters temperature
void fermenter::add_item_temperature2(QString temperature2)
{
    list_temperatura2.append(temperature2);
}
QStringList fermenter::get_list_temperature2()
{
    return list_temperatura2;
}
QString fermenter::get_item_list_temperature2(int position)
{
    return list_temperatura2.value(position);

}


void fermenter::add_item_raw_temperature2(QString temperature2)
{
    list_Raw_temperatura2.append(temperature2);
}
QStringList fermenter::get_list_raw_temperature2()
{
    return list_Raw_temperatura2;
}
QString fermenter::get_item_list_raw_temperature2(int position)
{
    return list_Raw_temperatura2.value(position);

}

//*****************************************************************************
//*****************************************************************************
//setters and getters temperature
void fermenter::add_item_temperature3(QString temperature3)
{
    list_temperatura3.append(temperature3);
}
QStringList fermenter::get_list_temperature3()
{
    return list_temperatura3;
}
QString fermenter::get_item_list_temperature3(int position)
{
    return list_temperatura3.value(position);

}


void fermenter::add_item_raw_temperature3(QString temperature3)
{
    list_Raw_temperatura3.append(temperature3);
}
QStringList fermenter::get_list_raw_temperature3()
{
    return list_Raw_temperatura3;
}
QString fermenter::get_item_list_raw_temperature3(int position)
{
    return list_Raw_temperatura3.value(position);

}

//*****************************************************************************
//*****************************************************************************
//setters and getters temperature
void fermenter::add_item_temperature4(QString temperature4)
{
    list_temperatura4.append(temperature4);
}
QStringList fermenter::get_list_temperature4()
{
    return list_temperatura4;
}
QString fermenter::get_item_list_temperature4(int position)
{
    return list_temperatura4.value(position);

}


void fermenter::add_item_raw_temperature4(QString temperature4)
{
    list_Raw_temperatura4.append(temperature4);
}
QStringList fermenter::get_list_raw_temperature4()
{
    return list_Raw_temperatura4;
}
QString fermenter::get_item_list_raw_temperature4(int position)
{
    return list_Raw_temperatura4.value(position);

}

//*****************************************************************************
//*****************************************************************************
//setters and getters temperature
void fermenter::add_item_alcohol(QString alcohol)
{
    list_alcohol.append(alcohol);
}
QStringList fermenter::get_list_alcohol()
{
    return list_alcohol;
}
QString fermenter::get_item_list_alcohol(int position)
{
    return list_alcohol.value(position);

}


void fermenter::add_item_raw_alcohol(QString alcohol)
{
    list_Raw_alcohol.append(alcohol);
}
QStringList fermenter::get_list_raw_alcohol()
{
    return list_Raw_alcohol;
}
QString fermenter::get_item_list_raw_alcohol(int alcohol)
{
    return list_Raw_alcohol.value(alcohol);

}

//*******************************************


//setters and getters temperature
void fermenter::add_item_methano(QString methano)
{
    list_methano.append(methano);
}
QStringList fermenter::get_list_methano()
{
    return list_methano;
}
QString fermenter::get_item_list_methano(int position)
{
    return list_methano.value(position);

}


void fermenter::add_item_raw_methano(QString methano)
{
    list_Raw_methano.append(methano);
}
QStringList fermenter::get_list_raw_methano()
{
    return list_Raw_methano;
}
QString fermenter::get_item_list_raw_methano(int methano)
{
    return list_Raw_methano.value(methano);

}

//*******************************************

int fermenter::count_elements()
{
    return list_Raw_ph.count();//se devuelve la cuanta de cualquier variable.

}
