
// las clases .h como su nombre lo indica (Headers o cabeceras)  tienenla funcion de denotar
//el modelo de datos de nuestra clase de manejo .cpp.

#include <QDebug>
#include <QFileDialog>
#include <QVBoxLayout>
#include <QCheckBox>
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>
#include <QDate>
#include <QDateEdit>
#include <QPalette>
#include <QColor>
#include <QColorDialog>
#include <QTimer>
#include <QElapsedTimer>
//*******************************************************************************


//******************************************************************************
#include "translator.h"
#include "ui_translator.h"

//defiunimos tres elementos de validacion de los datos obtenidos
QVector<QCheckBox *> check_niveles;
QVector<QCheckBox *> check_pluvio;
QVector<QCheckBox *> check_umbrales;
QVector<QCheckBox *> check_distanciometer;
QVector<QCheckBox *> check_fermenter;

//Estas variables referencia a las variables obtenidas por medio de la clase jptdabase.

QList<model_nodex> dataNodex;
//se necesita para mayor facilidad una lista de nombres de sensores registrados
//ejemplo:el didentificador de cada elemento es de la forma : 00600005 o 023000104;

QStringList lv_umbrales;//la lista comparte la posicion dentro del vector,
//ejemplo: si mi umbra es el 1 entonces seria el list_umbral[0],
//su nombre estaria en lv_umbales[0](label_vector_umbrales)
QStringList lv_niveles;
QStringList lv_pluvio;
QStringList lv_distanciometer;
QStringList lv_fermenter;


//se necesit una arquitectura de tipo pluviometro,nivel y umbral.


translator::translator(QWidget *parent) : QMainWindow(parent), ui(new Ui::translator)
{
    //Iniciamos la interfaz grafica
    ui->setupUi(this);

    //Creamos la conexion al proceso de abrir la base de datos
    connect(ui->btn_db_open,SIGNAL(clicked()),this,SLOT(select_db()));
    //conexion al sistema de test de la base de datos.. .
    connect(ui->btn_test_db,SIGNAL(clicked()),this,SLOT(test_db()));

    connect(ui->db_direction,SIGNAL(textChanged(const QString &)),this,SLOT(change_estate_dir(const QString &)));

    connect(ui->date1, SIGNAL(dateChanged(QDate)),this,SLOT(dateStartSlot(QDate)));

    connect(ui->date2, SIGNAL(dateChanged(const QDate &)),this,SLOT(dateEndSlot(const QDate &)));

    connect(ui->ParseButton,SIGNAL(clicked()),this,SLOT(parse_data()));

    connect(this, SIGNAL(msgConsole(QString)), this, SLOT(sendMsgConsole(QString)));

    ui->date1->setEnabled(false);
    ui->date2->setEnabled(false);

    //COLOR CONSOLE
    QPalette paletteConsole = ui->console->palette();
    paletteConsole.setColor(QPalette::Active, QPalette::Base, Qt::black);
    ui->console->setTextColor(QColor("white"));
    ui->console->setPalette(paletteConsole);
}


void translator::sendMsgConsole(QString msg){
    qDebug()<<"Emitio "<<msg;
    ui->console->append(msg);
}

translator::~translator()
{
    delete ui;
}
//este proceso guarda la direccion donde esta almacenada la base de datos.
void translator::select_db()
{
    ui->btn_test_db->setEnabled(false);
    //Seleccionamos la base de datos y guardamos la direccion en el lineEdit.
    QString file_name=QFileDialog::getOpenFileName(this,"Open a DB","/home/",tr("SQlite DB(*.db)"));

    //validamos el path seleccionado y mostramos en pantalla
    if(file_name!="")
    {
        ui->db_direction->setText(file_name);
        // ui->btn_test_db->setEnabled(true);
    }


}

void translator::test_db()
{
    db= new jptdatabase();
    //para la coneccion se entrega 1 parametro, que es el nombre de la conneccion.
    QString name_connection;

    name_connection=ui->db_direction->text();// se obtuvo por medio del sistema select_db()

    if(name_connection!="")
    {
        emit msgConsole("Connecting Database");
        db->init_database_connection(name_connection);
        //si la conexion se realizo entonces genero el espacio en memoria con la informacion
        try {
            db->charge_data_on_memory(getDateStart(), getDateEnd());
            //si ya se cargo la data ahora trato de cargar la informacion en el sistema actual
            try {
                dataNodex.clear();
                dataNodex = db->getDataAllNodex();
                //Se debe hacer lo siguiente:
                // se debe listar dentro de cuadro de dialogo todos los sensores que existen dentro de la base de datos.
                //para ellos se debe de usar el parametro device de la lista o clase que se genere.
                //si en este punto todo funciona bien, entonces se debe de inspeccionar la base de datos.
                inspecct_db();
            } catch (...) {
                qDebug()<<"Error getting locals variables";
            }
        } catch (...) {
            qDebug()<<"The system couldn't charge the database info.";
        }
    }
    else {
        qDebug()<<"Dabatabse Error: Direction Error.";
    }


    //Prueba la base de datos y lista los diferentes sensores encontrados en ella.
    QVBoxLayout * lay= new QVBoxLayout(this);

    if(list_niveles.count()!=0)
    {

        for (int i=0;i<list_niveles.count();i++) {
            QCheckBox *_check=new QCheckBox("Nivel: "+list_niveles[i].get_id_device());
            check_niveles.append(_check);
            lay->addWidget(check_niveles[i]);
        }
    }
    if(list_pluvio.count()!=0)
    {

        for (int i=0;i<list_pluvio.count();i++) {
            QCheckBox *_check=new QCheckBox("Pluviometro: "+list_pluvio[i].get_id_device());
            check_pluvio.append(_check);
            lay->addWidget(check_pluvio[i]);
        }
    }
    if(list_umbrales.count()!=0)
    {

        for (int i=0;i<list_umbrales.count();i++) {
            QCheckBox *_check=new QCheckBox("Umbrales: "+list_umbrales[i].get_id_device());
            check_umbrales.append(_check);
            lay->addWidget(check_umbrales[i]);
        }
    }
    if(list_distan.count()!=0)
    {

        for (int i=0;i<list_distan.count();i++) {
            QCheckBox *_check=new QCheckBox("Distanciometros: "+list_distan[i].get_id_device());
            check_distanciometer.append(_check);
            lay->addWidget(check_distanciometer[i]);
        }
    }

    if(list_fermenter.count()!=0)
    {

        for (int i=0;i<list_fermenter.count();i++) {
            QCheckBox *_check=new QCheckBox("Fermentadores: "+list_fermenter[i].get_id_device());
            check_fermenter.append(_check);
            lay->addWidget(check_fermenter[i]);
        }
    }

    ui->scrollcontent->setLayout(lay);

}

void translator::inspecct_db()
{   //proceso encargado de parsear la informacion de la base de datos
    //para ello recorreoms toda la data obtenida.
    for (int inspector_var=0;inspector_var<dataNodex.count();inspector_var++) {
        //        qDebug()<<
        //primero  probamos el elemento parseador de json
        //se recuerda m_data, es una lista de tamaño N la cual contiene en cada uno de sus elementos los json de informacion.
        QString dataJson=dataNodex.value(inspector_var).getData();
        //creamos un tipo de elemento documentjson el cual usara nuestra data que teneos como string almacenada.
        QJsonDocument jsonResponse =QJsonDocument::fromJson(dataJson.toUtf8());//el formato usado por defecto en c++                                                                      //para strings es ascci pero json document lee utf8.

        QJsonObject jsonObject=jsonResponse.object();

        QJsonObject jsonDevice=jsonObject.value("device").toObject();

        QString id_device = jsonDevice.value("device_id").toString();

        int opcion = id_device.toInt();

        QJsonObject jsonSensors=jsonDevice.value("sensors").toObject();

        switch (opcion) {
        case 1:
        {
            //qDebug()<<"sensor de pluviometro";

            //ahora se parsea cada sensor

            QJsonObject jsonPresipitation=jsonSensors.value("1-precipitation").toObject();
            QJsonObject jsonAcumulada=jsonSensors.value("2-accumulated-precipitation").toObject();
            QJsonObject jsonHumidity=jsonSensors.value("3-humidity").toObject();
            QJsonObject jsonTemperature=jsonSensors.value("4-temperature").toObject();
            QJsonObject jsonPresure=jsonSensors.value("5-atmosferic-presure").toObject();


            /*
               qDebug()<<jsonAccX.value("ENG").toString();
               qDebug()<<jsonAccY.value("ENG").toString();
               qDebug()<<jsonAccZ.value("ENG").toString();
               qDebug()<<jsonHumidity.value("ENG").toString();
               qDebug()<<jsonVibX.value("ENG").toString();
               qDebug()<<jsonVibY.value("ENG").toString();
               qDebug()<<jsonVibZ.value("ENG").toString();
               */

            //al obteneer los sensores se debe verificar ahora que equipo es y agregarlo donde va.
            //qDebug()<<list_umbrales.count();
            if(list_pluvio.count()==0)//list_pluvio es un Qvector<pluvio>
            {
                lv_pluvio.append(jsonDevice.value("board_id").toString());
                pluvio new_pluvio;
                new_pluvio.setID(dataNodex.value(inspector_var).getID());
                new_pluvio.set_id_device(jsonDevice.value("board_id").toString());
                //seteeamos sensores , valores de ingenieria.
                new_pluvio.add_item_precipitation(jsonPresipitation.value("ENG").toString());
                new_pluvio.add_item_acumulada(jsonAcumulada.value("ENG").toString());;
                new_pluvio.add_item_humidity(jsonHumidity.value("ENG").toString());
                new_pluvio.add_item_temperature(jsonTemperature.value("ENG").toString());;
                new_pluvio.add_item_presure(jsonPresure.value("ENG").toString());;

                //seteeamos sensores, valores RAW

                new_pluvio.add_item_raw_precipitation(jsonPresipitation.value("RAW").toString());
                new_pluvio.add_item_raw_acumulada(jsonAcumulada.value("RAW").toString());
                new_pluvio.add_item_raw_humidity(jsonHumidity.value("RAW").toString());
                new_pluvio.add_item_raw_temperature(jsonTemperature.value("RAW").toString());;
                new_pluvio.add_item_raw_presure(jsonPresure.value("RAW").toString());;

                //seteeamos el tiempo
                new_pluvio.add_item_time(dataNodex.value(inspector_var).getDateTime());

                //este nuevo valor se agrega un mi lista de vectror, como el elmento 1 del vector con una fila.
                list_pluvio.append(new_pluvio);
            }
            else {
                bool bandera_exist=false;//verifica si existe la tabla ya creada.
                int position_bandera_pluvio=10000;
                for (int var=0;var<lv_pluvio.count();var++) {
                    if(lv_pluvio.value(var)==jsonDevice.value("board_id").toString())
                    {
                        //si exte un valor similar, entonces seteo el bool en true.
                        bandera_exist=true;
                        position_bandera_pluvio=var;
                    }
                }
                //Nota: En la lista de nombres lv_pluvio, el verifica si el nombre ya existe, si esto sucede,
                //significa que ya existe un Qvector<pluvio> creado para almacenar los datos de este dispositivo.
                //Nota2.  Como se tiene una variable de posicion del vector la cual nunca puede ser null,
                //dad0 que ello ocacionaria un error asignamos un valor el cual nunca podria suceder.
                if(bandera_exist==true && position_bandera_pluvio!=10000)
                {
                    //si existe entonces agrego un nuevo valor a la pila existente.

                    //seteeamos sensores , valores de ingenieria.
                    list_pluvio[position_bandera_pluvio].add_item_precipitation(jsonPresipitation.value("ENG").toString());
                    list_pluvio[position_bandera_pluvio].add_item_acumulada(jsonAcumulada.value("ENG").toString());
                    list_pluvio[position_bandera_pluvio].add_item_humidity(jsonHumidity.value("ENG").toString());
                    list_pluvio[position_bandera_pluvio].add_item_temperature(jsonTemperature.value("ENG").toString());
                    list_pluvio[position_bandera_pluvio].add_item_presure(jsonPresure.value("ENG").toString());

                    //seteeamos sensores, valores RAW

                    list_pluvio[position_bandera_pluvio].add_item_raw_precipitation(jsonPresipitation.value("RAW").toString());
                    list_pluvio[position_bandera_pluvio].add_item_raw_acumulada(jsonAcumulada.value("RAW").toString());
                    list_pluvio[position_bandera_pluvio].add_item_raw_humidity(jsonHumidity.value("RAW").toString());
                    list_pluvio[position_bandera_pluvio].add_item_raw_temperature(jsonTemperature.value("RAW").toString());
                    list_pluvio[position_bandera_pluvio].add_item_raw_presure(jsonPresure.value("RAW").toString());

                    //seteeamos el tiempo
                    list_pluvio[position_bandera_pluvio].add_item_time(dataNodex.value(inspector_var).getDateTime());
                }
                else {
                    //Nota: si en caso contrario el Qvector no tiene al macenado un elemento con ese id de dispositvo
                    // se procede a agregar uno.
                    //como el valor no existe debo de crear una nueva entrada.
                    lv_pluvio.append(jsonDevice.value("board_id").toString());
                    //qDebug()<<lv_umbrales.count();
                    pluvio new_pluvio;
                    new_pluvio.setID(dataNodex.value(inspector_var).getID());
                    new_pluvio.set_id_device(jsonDevice.value("board_id").toString());
                    //seteeamos sensores , valores de ingenieria.
                    new_pluvio.add_item_precipitation(jsonPresipitation.value("ENG").toString());
                    new_pluvio.add_item_acumulada(jsonAcumulada.value("ENG").toString());
                    new_pluvio.add_item_humidity(jsonHumidity.value("ENG").toString());
                    new_pluvio.add_item_temperature(jsonTemperature.value("ENG").toString());;
                    new_pluvio.add_item_presure(jsonPresure.value("ENG").toString());;


                    //seteeamos sensores, valores RAW

                    new_pluvio.add_item_raw_precipitation(jsonPresipitation.value("RAW").toString());
                    new_pluvio.add_item_raw_acumulada(jsonAcumulada.value("RAW").toString());
                    new_pluvio.add_item_raw_humidity(jsonHumidity.value("RAW").toString());
                    new_pluvio.add_item_raw_temperature(jsonTemperature.value("RAW").toString());;
                    new_pluvio.add_item_raw_presure(jsonPresure.value("RAW").toString());;



                    //seteeamos el tiempo
                    new_pluvio.add_item_time(dataNodex.value(inspector_var).getDateTime());


                    list_pluvio.append(new_pluvio);
                }
            }
        }
            break;
        case 2:
        {
            //qDebug()<<"sensor de nivel";
            //ahora se parsea cada sensor

            QJsonObject jsonTemperature=jsonSensors.value("1-temperature").toObject();
            QJsonObject jsonNiveles=jsonSensors.value("2-level").toObject();
            QJsonObject jsonVibration=jsonSensors.value("3-threshold-vibration").toObject();
            QJsonObject jsonSound=jsonSensors.value("4-threshold-sound").toObject();

            //al obteneer los sensores se debe verificar ahora que equipo es y agregarlo donde va.
            //qDebug()<<list_umbrales.count();
            if(list_niveles.count()==0)
            {
                lv_niveles.append(jsonDevice.value("board_id").toString());
                //qDebug()<<lv_umbrales.count();
                nivel new_nivel;
                new_nivel.setID(dataNodex.value(inspector_var).getID());
                new_nivel.set_id_device(jsonDevice.value("board_id").toString());
                //seteeamos sensores , valores de ingenieria.
                new_nivel.add_item_nivel(jsonNiveles.value("ENG").toString());
                new_nivel.add_item_temperature(jsonTemperature.value("ENG").toString());;
                new_nivel.add_item_vibration(jsonVibration.value("ENG").toString());
                new_nivel.add_item_sound(jsonSound.value("ENG").toString());;

                //seteeamos sensores, valores RAW

                new_nivel.add_item_raw_nivel(jsonNiveles.value("RAW").toString());
                new_nivel.add_item_raw_temperature(jsonTemperature.value("RAW").toString());
                new_nivel.add_item_raw_vibration(jsonVibration.value("RAW").toString());
                new_nivel.add_item_raw_sound(jsonSound.value("RAW").toString());;


                //seteeamos el tiempo
                new_nivel.add_item_time(dataNodex.value(inspector_var).getDateTime());


                list_niveles.append(new_nivel);
            }
            else {
                bool bandera_exist=false;
                int position_bandera_nivel=10000;
                for (int var=0;var<lv_niveles.count();var++) {
                    if(lv_niveles.value(var)==jsonDevice.value("board_id").toString())
                    {
                        //si exte un valor similar, entonces seteo el bool en true.
                        bandera_exist=true;
                        position_bandera_nivel=var;
                    }
                }
                if(bandera_exist==true && position_bandera_nivel!=10000)
                {
                    //si existe entonces agrego un nuevo valor a la pila existente.

                    //seteeamos sensores , valores de ingenieria.
                    list_niveles[position_bandera_nivel].add_item_nivel(jsonNiveles.value("ENG").toString());
                    list_niveles[position_bandera_nivel].add_item_temperature(jsonTemperature.value("ENG").toString());
                    list_niveles[position_bandera_nivel].add_item_vibration(jsonVibration.value("ENG").toString());
                    list_niveles[position_bandera_nivel].add_item_sound(jsonSound.value("ENG").toString());


                    //seteeamos sensores, valores RAW

                    list_niveles[position_bandera_nivel].add_item_raw_nivel(jsonNiveles.value("RAW").toString());
                    list_niveles[position_bandera_nivel].add_item_raw_temperature(jsonTemperature.value("RAW").toString());
                    list_niveles[position_bandera_nivel].add_item_raw_vibration(jsonVibration.value("RAW").toString());
                    list_niveles[position_bandera_nivel].add_item_raw_sound(jsonSound.value("RAW").toString());


                    //seteeamos el tiempo
                    list_niveles[position_bandera_nivel].add_item_time(dataNodex.value(inspector_var).getDateTime());
                }
                else {
                    //como el valor no existe debo de crear una nueva entrada.
                    lv_niveles.append(jsonDevice.value("board_id").toString());
                    //qDebug()<<lv_umbrales.count();
                    nivel new_nivel;
                    new_nivel.setID(dataNodex.value(inspector_var).getID());
                    new_nivel.set_id_device(jsonDevice.value("board_id").toString());
                    //seteeamos sensores , valores de ingenieria.
                    new_nivel.add_item_nivel(jsonNiveles.value("ENG").toString());
                    new_nivel.add_item_temperature(jsonTemperature.value("ENG").toString());
                    new_nivel.add_item_vibration(jsonVibration.value("ENG").toString());
                    new_nivel.add_item_sound(jsonSound.value("ENG").toString());;

                    //seteeamos sensores, valores RAW

                    new_nivel.add_item_raw_nivel(jsonNiveles.value("RAW").toString());
                    new_nivel.add_item_raw_temperature(jsonTemperature.value("RAW").toString());
                    new_nivel.add_item_raw_vibration(jsonVibration.value("RAW").toString());
                    new_nivel.add_item_raw_sound(jsonSound.value("RAW").toString());;


                    //seteeamos el tiempo
                    new_nivel.add_item_time(dataNodex.value(inspector_var).getDateTime());


                    list_niveles.append(new_nivel);
                }
            }
        }
            //qDebug()<<list_umbrales.count();
            break;
        case 3:
        {
            //qDebug()<<"umbral de suelos";
            //ahora se parsea cada sensor
            QJsonObject jsonAccX=jsonSensors.value("1-Acc-X").toObject();
            QJsonObject jsonAccY=jsonSensors.value("2-Acc-Y").toObject();
            QJsonObject jsonAccZ=jsonSensors.value("3-Acc-Z").toObject();
            QJsonObject jsonHumidity=jsonSensors.value("4-humidity").toObject();
            QJsonObject jsonVibX=jsonSensors.value("5-Vibration-X").toObject();
            QJsonObject jsonVibY=jsonSensors.value("6-Vibration-Y").toObject();
            QJsonObject jsonVibZ=jsonSensors.value("7-Vibration-Z").toObject();

            //al obteneer los sensores se debe verificar ahora que equipo es y agregarlo donde va.
            //qDebug()<<list_umbrales.count();
            if(list_umbrales.count()==0)
            {
                lv_umbrales.append(jsonDevice.value("board_id").toString());
                //qDebug()<<lv_umbrales.count();
                umbral new_umbral;
                new_umbral.setID(dataNodex.value(inspector_var).getID());
                new_umbral.set_id_device(jsonDevice.value("board_id").toString());
                //seteeamos sensores , valores de ingenieria.
                new_umbral.add_item_acc_x(jsonAccX.value("ENG").toString());
                new_umbral.add_item_acc_y(jsonAccY.value("ENG").toString());
                new_umbral.add_item_acc_z(jsonAccZ.value("ENG").toString());
                new_umbral.add_item_hum(jsonHumidity.value("ENG").toString());
                new_umbral.add_item_vib_x(jsonVibX.value("ENG").toString());
                new_umbral.add_item_vib_y(jsonVibY.value("ENG").toString());
                new_umbral.add_item_vib_z(jsonVibZ.value("ENG").toString());

                //seteeamos sensores, valores RAW

                new_umbral.add_item_raw_acc_x(jsonAccX.value("RAW").toString());
                new_umbral.add_item_raw_acc_y(jsonAccY.value("RAW").toString());
                new_umbral.add_item_raw_acc_z(jsonAccZ.value("RAW").toString());
                new_umbral.add_item_raw_hum(jsonHumidity.value("RAW").toString());
                new_umbral.add_item_raw_vib_x(jsonVibX.value("RAW").toString());
                new_umbral.add_item_raw_vib_y(jsonVibY.value("RAW").toString());
                new_umbral.add_item_raw_vib_z(jsonVibZ.value("RAW").toString());

                //seteeamos el tiempo
                new_umbral.add_item_time(dataNodex.value(inspector_var).getDateTime());

                list_umbrales.append(new_umbral);
            }
            else {
                bool bandera_exist=false;
                int position_bandera_umbral=10000;
                for (int var=0;var<lv_umbrales.count();var++) {
                    if(lv_umbrales.value(var)==jsonDevice.value("board_id").toString())
                    {
                        //si exte un valor similar, entonces seteo el bool en true.
                        bandera_exist=true;
                        position_bandera_umbral=var;
                    }
                }
                if(bandera_exist==true && position_bandera_umbral!=10000)
                {
                    //si existe entonces agrego un nuevo valor a la pila existente.

                    //seteeamos sensores , valores de ingenieria.
                    list_umbrales[position_bandera_umbral].add_item_acc_x(jsonAccX.value("ENG").toString());
                    list_umbrales[position_bandera_umbral].add_item_acc_y(jsonAccY.value("ENG").toString());
                    list_umbrales[position_bandera_umbral].add_item_acc_z(jsonAccZ.value("ENG").toString());
                    list_umbrales[position_bandera_umbral].add_item_hum(jsonHumidity.value("ENG").toString());
                    list_umbrales[position_bandera_umbral].add_item_vib_x(jsonVibX.value("ENG").toString());
                    list_umbrales[position_bandera_umbral].add_item_vib_y(jsonVibY.value("ENG").toString());
                    list_umbrales[position_bandera_umbral].add_item_vib_z(jsonVibZ.value("ENG").toString());

                    //seteeamos sensores, valores RAW

                    list_umbrales[position_bandera_umbral].add_item_raw_acc_x(jsonAccX.value("RAW").toString());
                    list_umbrales[position_bandera_umbral].add_item_raw_acc_y(jsonAccY.value("RAW").toString());
                    list_umbrales[position_bandera_umbral].add_item_raw_acc_z(jsonAccZ.value("RAW").toString());
                    list_umbrales[position_bandera_umbral].add_item_raw_hum(jsonHumidity.value("RAW").toString());
                    list_umbrales[position_bandera_umbral].add_item_raw_vib_x(jsonVibX.value("RAW").toString());
                    list_umbrales[position_bandera_umbral].add_item_raw_vib_y(jsonVibY.value("RAW").toString());
                    list_umbrales[position_bandera_umbral].add_item_raw_vib_z(jsonVibZ.value("RAW").toString());


                    //seteeamos el tiempo
                    list_umbrales[position_bandera_umbral].add_item_time(dataNodex.value(inspector_var).getDateTime());
                }
                else {
                    //como el valor no existe debo de crear una nueva entrada.
                    lv_umbrales.append(jsonDevice.value("board_id").toString());
                    //qDebug()<<lv_umbrales.count();
                    umbral new_umbral;
                    new_umbral.setID(dataNodex.value(inspector_var).getID());
                    new_umbral.set_id_device(jsonDevice.value("board_id").toString());
                    //seteeamos sensores , valores de ingenieria.
                    new_umbral.add_item_acc_x(jsonAccX.value("ENG").toString());
                    new_umbral.add_item_acc_y(jsonAccY.value("ENG").toString());
                    new_umbral.add_item_acc_z(jsonAccZ.value("ENG").toString());
                    new_umbral.add_item_hum(jsonHumidity.value("ENG").toString());
                    new_umbral.add_item_vib_x(jsonVibX.value("ENG").toString());
                    new_umbral.add_item_vib_y(jsonVibY.value("ENG").toString());
                    new_umbral.add_item_vib_z(jsonVibZ.value("ENG").toString());

                    //seteeamos sensores, valores RAW

                    new_umbral.add_item_raw_acc_x(jsonAccX.value("RAW").toString());
                    new_umbral.add_item_raw_acc_y(jsonAccY.value("RAW").toString());
                    new_umbral.add_item_raw_acc_z(jsonAccZ.value("RAW").toString());
                    new_umbral.add_item_raw_hum(jsonHumidity.value("RAW").toString());
                    new_umbral.add_item_raw_vib_x(jsonVibX.value("RAW").toString());
                    new_umbral.add_item_raw_vib_y(jsonVibY.value("RAW").toString());
                    new_umbral.add_item_raw_vib_z(jsonVibZ.value("RAW").toString());

                    //seteeamos el tiempo
                    new_umbral.add_item_time(dataNodex.value(inspector_var).getDateTime());
                    list_umbrales.append(new_umbral);
                }
            }
            //qDebug()<<list_umbrales.count();
            break;
        }

        case 11:{
            //Distanciometer = id_device: 11
            QJsonObject jsonDistanciometer=jsonSensors.value("1-distance").toObject();

            //al obteneer los sensores se debe verificar ahora que equipo es y agregarlo donde va.
            //qDebug()<<list_umbrales.count();
            if(list_distan.count()==0)//list_pluvio es un Qvector<pluvio>
            {
                lv_distanciometer.append(jsonDevice.value("board_id").toString());
                distanciometer new_distanciometer;
                new_distanciometer.setID(dataNodex.value(inspector_var).getID());
                new_distanciometer.set_id_device(jsonDevice.value("board_id").toString());
                //seteeamos sensores , valores de ingenieria.
                new_distanciometer.add_item_distanciometer(jsonDistanciometer.value("ENG").toString());
                //seteeamos sensores, valores RAW
                new_distanciometer.add_item_raw_distanciometer(jsonDistanciometer.value("RAW").toString());
                //seteeamos el tiempo
                new_distanciometer.add_item_time(dataNodex.value(inspector_var).getDateTime());

                //este nuevo valor se agrega un mi lista de vectror, como el elmento 1 del vector con una fila.
                list_distan.append(new_distanciometer);
            }
            else {
                bool bandera_exist=false;//verifica si existe la tabla ya creada.
                int position_bandera_distan=10000;
                for (int var=0;var<lv_distanciometer.count();var++) {
                    if(lv_distanciometer.value(var)==jsonDevice.value("board_id").toString())
                    {
                        //si exte un valor similar, entonces seteo el bool en true.
                        bandera_exist=true;
                        position_bandera_distan=var;
                    }
                }
                //Nota: En la lista de nombres lv_distanciometer, el verifica si el nombre ya existe, si esto sucede,
                //significa que ya existe un Qvector<distanciometer> creado para almacenar los datos de este dispositivo.
                //Nota2.  Como se tiene una variable de posicion del vector la cual nunca puede ser null,
                //dad0 que ello ocacionaria un error asignamos un valor el cual nunca podria suceder.
                if(bandera_exist==true && position_bandera_distan!=10000)
                {
                    //si existe entonces agrego un nuevo valor a la pila existente.

                    //seteeamos sensores , valores de ingenieria.
                    list_distan[position_bandera_distan].add_item_distanciometer(jsonDistanciometer.value("ENG").toString());
                    //seteeamos sensores, valores RAW
                    list_distan[position_bandera_distan].add_item_raw_distanciometer(jsonDistanciometer.value("RAW").toString());
                    //seteeamos el tiempo
                    list_distan[position_bandera_distan].add_item_time(dataNodex.value(inspector_var).getDateTime());
                }
                else {
                    //Nota: si en caso contrario el Qvector no tiene al macenado un elemento con ese id de dispositvo
                    // se procede a agregar uno.
                    //como el valor no existe debo de crear una nueva entrada.
                    lv_distanciometer.append(jsonDevice.value("board_id").toString());
                    //qDebug()<<lv_umbrales.count();
                    distanciometer new_distan;
                    new_distan.setID(dataNodex.value(inspector_var).getID());
                    new_distan.set_id_device(jsonDevice.value("board_id").toString());
                    //seteeamos sensores , valores de ingenieria.
                    new_distan.add_item_distanciometer(jsonDistanciometer.value("ENG").toString());
                    //seteeamos sensores, valores RAW
                    new_distan.add_item_raw_distanciometer(jsonDistanciometer.value("RAW").toString());
                    //seteeamos el tiempo
                    new_distan.add_item_time(dataNodex.value(inspector_var).getDateTime());

                    list_distan.append(new_distan);
                }
            }

            break;
        }

        case 13:
        {
            //qDebug()<<"umbral de suelos";
            //ahora se parsea cada sensor
            QJsonObject jsonPh=jsonSensors.value("1-ph").toObject();
            QJsonObject jsonResistivity1=jsonSensors.value("2-resistivity-1").toObject();
            QJsonObject jsonResistivity2=jsonSensors.value("3-resistivity-2").toObject();
            QJsonObject jsonTemperature1=jsonSensors.value("4-temperature-1").toObject();
            QJsonObject jsonTemperature2=jsonSensors.value("5-temperature-2").toObject();
            QJsonObject jsonTemperature3=jsonSensors.value("6-temperature-3").toObject();
            QJsonObject jsonTemperature4=jsonSensors.value("7-temperature-4").toObject();
            QJsonObject jsonAlcohol=jsonSensors.value("8-alcohol").toObject();
            QJsonObject jsonMethano=jsonSensors.value("9-methane").toObject();

            //al obteneer los sensores se debe verificar ahora que equipo es y agregarlo donde va.
            //qDebug()<<list_umbrales.count();
            if(list_fermenter.count()==0)
            {
                lv_fermenter.append(jsonDevice.value("board_id").toString());
                //qDebug()<<lv_umbrales.count();
                fermenter new_fermenter;
                new_fermenter.setID(dataNodex.value(inspector_var).getID());
                new_fermenter.set_id_device(jsonDevice.value("board_id").toString());
                //seteeamos sensores , valores de ingenieria.
                new_fermenter.add_item_ph(jsonPh.value("ENG").toString());
                new_fermenter.add_item_resistivity1(jsonResistivity1.value("ENG").toString());
                new_fermenter.add_item_resistivity2(jsonResistivity2.value("ENG").toString());
                new_fermenter.add_item_temperature1(jsonTemperature1.value("ENG").toString());
                new_fermenter.add_item_temperature2(jsonTemperature2.value("ENG").toString());
                new_fermenter.add_item_temperature3(jsonTemperature3.value("ENG").toString());
                new_fermenter.add_item_temperature4(jsonTemperature4.value("ENG").toString());
                new_fermenter.add_item_alcohol(jsonAlcohol.value("ENG").toString());
                new_fermenter.add_item_methano(jsonMethano.value("ENG").toString());

                //seteeamos sensores, valores RAW

                new_fermenter.add_item_raw_ph(jsonPh.value("RAW").toString());
                new_fermenter.add_item_raw_resistivity1(jsonResistivity1.value("RAW").toString());
                new_fermenter.add_item_raw_resistivity2(jsonResistivity2.value("RAW").toString());
                new_fermenter.add_item_raw_temperature1(jsonTemperature1.value("RAW").toString());
                new_fermenter.add_item_raw_temperature2(jsonTemperature2.value("RAW").toString());
                new_fermenter.add_item_raw_temperature3(jsonTemperature3.value("RAW").toString());
                new_fermenter.add_item_raw_temperature4(jsonTemperature4.value("RAW").toString());
                new_fermenter.add_item_raw_alcohol(jsonAlcohol.value("RAW").toString());
                new_fermenter.add_item_raw_methano(jsonMethano.value("RAW").toString());

                //seteeamos el tiempo
                new_fermenter.add_item_time(dataNodex.value(inspector_var).getDateTime());

                list_fermenter.append(new_fermenter);
            }
            else {
                bool bandera_exist=false;
                int position_bandera_fermenter=10000;
                for (int var=0;var<lv_fermenter.count();var++) {
                    if(lv_fermenter.value(var)==jsonDevice.value("board_id").toString())
                    {
                        //si exte un valor similar, entonces seteo el bool en true.
                        bandera_exist=true;
                        position_bandera_fermenter=var;
                    }
                }
                if(bandera_exist==true && position_bandera_fermenter!=10000)
                {
                    //si existe entonces agrego un nuevo valor a la pila existente.

                    //seteeamos sensores , valores de ingenieria.
                    list_fermenter[position_bandera_fermenter].add_item_ph(jsonPh.value("ENG").toString());
                    list_fermenter[position_bandera_fermenter].add_item_resistivity1(jsonResistivity1.value("ENG").toString());
                    list_fermenter[position_bandera_fermenter].add_item_resistivity2(jsonResistivity2.value("ENG").toString());
                    list_fermenter[position_bandera_fermenter].add_item_temperature1(jsonTemperature1.value("ENG").toString());
                    list_fermenter[position_bandera_fermenter].add_item_temperature2(jsonTemperature2.value("ENG").toString());
                    list_fermenter[position_bandera_fermenter].add_item_temperature3(jsonTemperature3.value("ENG").toString());
                    list_fermenter[position_bandera_fermenter].add_item_temperature4(jsonTemperature4.value("ENG").toString());
                    list_fermenter[position_bandera_fermenter].add_item_alcohol(jsonAlcohol.value("ENG").toString());
                    list_fermenter[position_bandera_fermenter].add_item_methano(jsonMethano.value("ENG").toString());

                    //seteeamos sensores, valores RAW

                    list_fermenter[position_bandera_fermenter].add_item_raw_ph(jsonPh.value("RAW").toString());
                    list_fermenter[position_bandera_fermenter].add_item_raw_resistivity1(jsonResistivity1.value("RAW").toString());
                    list_fermenter[position_bandera_fermenter].add_item_raw_resistivity2(jsonResistivity2.value("RAW").toString());
                    list_fermenter[position_bandera_fermenter].add_item_raw_temperature1(jsonTemperature1.value("RAW").toString());
                    list_fermenter[position_bandera_fermenter].add_item_raw_temperature2(jsonTemperature2.value("RAW").toString());
                    list_fermenter[position_bandera_fermenter].add_item_raw_temperature3(jsonTemperature3.value("RAW").toString());
                    list_fermenter[position_bandera_fermenter].add_item_raw_temperature4(jsonTemperature4.value("RAW").toString());
                    list_fermenter[position_bandera_fermenter].add_item_raw_alcohol(jsonAlcohol.value("RAW").toString());
                    list_fermenter[position_bandera_fermenter].add_item_raw_methano(jsonMethano.value("RAW").toString());



                    //seteeamos el tiempo
                    list_fermenter[position_bandera_fermenter].add_item_time(dataNodex.value(inspector_var).getDateTime());
                }
                else {
                    lv_fermenter.append(jsonDevice.value("board_id").toString());
                    //qDebug()<<lv_umbrales.count();
                    fermenter new_fermenter;
                    new_fermenter.setID(dataNodex.value(inspector_var).getID());
                    new_fermenter.set_id_device(jsonDevice.value("board_id").toString());
                    //seteeamos sensores , valores de ingenieria.
                    new_fermenter.add_item_ph(jsonPh.value("ENG").toString());
                    new_fermenter.add_item_resistivity1(jsonResistivity1.value("ENG").toString());
                    new_fermenter.add_item_resistivity2(jsonResistivity2.value("ENG").toString());
                    new_fermenter.add_item_temperature1(jsonTemperature1.value("ENG").toString());
                    new_fermenter.add_item_temperature2(jsonTemperature2.value("ENG").toString());
                    new_fermenter.add_item_temperature3(jsonTemperature3.value("ENG").toString());
                    new_fermenter.add_item_temperature4(jsonTemperature4.value("ENG").toString());
                    new_fermenter.add_item_alcohol(jsonAlcohol.value("ENG").toString());
                    new_fermenter.add_item_methano(jsonMethano.value("ENG").toString());

                    //seteeamos sensores, valores RAW

                    new_fermenter.add_item_raw_ph(jsonPh.value("RAW").toString());
                    new_fermenter.add_item_raw_resistivity1(jsonResistivity1.value("RAW").toString());
                    new_fermenter.add_item_raw_resistivity2(jsonResistivity2.value("RAW").toString());
                    new_fermenter.add_item_raw_temperature1(jsonTemperature1.value("RAW").toString());
                    new_fermenter.add_item_raw_temperature2(jsonTemperature2.value("RAW").toString());
                    new_fermenter.add_item_raw_temperature3(jsonTemperature3.value("RAW").toString());
                    new_fermenter.add_item_raw_temperature4(jsonTemperature4.value("RAW").toString());
                    new_fermenter.add_item_raw_alcohol(jsonAlcohol.value("RAW").toString());
                    new_fermenter.add_item_raw_methano(jsonMethano.value("RAW").toString());

                    //seteeamos el tiempo
                    new_fermenter.add_item_time(dataNodex.value(inspector_var).getDateTime());

                    list_fermenter.append(new_fermenter);
                }
            }
            //qDebug()<<list_umbrales.count();
            break;
        }

        }
    }

    //****************************************************************************************************
    //****************************************************************************************************
    ui->console->append("# de Pluviometros: "+ QString::number(list_pluvio.count()));
    ui->console->append("# de Niveles: " + QString::number(list_niveles.count()));
    ui->console->append("# de Umbrales: " + QString::number(list_umbrales.count()));
    ui->console->append("# de Distanciometros: " + QString::number(list_distan.count()));
    ui->console->append("# de Fermentadores: " + QString::number(list_fermenter.count()));
    qDebug()<<list_umbrales.count();
    qDebug()<<list_niveles.count();
    qDebug()<<list_pluvio.count();
    qDebug()<<list_distan.count();
    qDebug()<<list_fermenter.count();

    //si todo funciono bien entonces habilito el sistema de parseo
    ui->ParseButton->setEnabled(true);

}

void translator::parse_data()
{

    //****************************************************************************************************
    //Ya con la data almacenada en tablas, o exactamente dentro de los Qvector correspondiente, se procede a:
    //obtener la direccion de salida de la respuesta con la data obtenida.

    //se obtine la direccion de almacenamiento de la informacion.
    //generar los archivos de cada tabla en esta direccion.

    QStringList parse_direction= ui->db_direction->text().split("/");
    QString direction_save="/";
    for(int p_list=0;p_list<parse_direction.count()-1;p_list++)
    {
        direction_save=direction_save+"/"+parse_direction.value(p_list);
    }


    //***********************************************************************************************
    //Luego se genera en el sistema un archivo csv con la informacion requerida.

    //***********************************************************************************************
    //**********************************PARSEO DE DATOS PLUVIO***************************************
    //***********************************************************************************************
    for (int parse=0;parse<list_pluvio.count();parse++) {

        if(check_pluvio[parse]->isChecked())
        {
            QFile pluvio_file(direction_save+"/"+list_pluvio[parse].get_id_device()+".csv");
            if(pluvio_file.open(QIODevice::ReadWrite))
            {
                QTextStream stream( &pluvio_file);
                stream << "time_stamp"<< "\t" << "1-Precipitation(RAW)"<<"\t"<<"1-Precipitation(ENG)"<< "\t"
                       << "2-Accumulated Precipitation(RAW)"<<"\t"<<"2-Accumulated Precipitation(ENG)" <<
                          "\t" << "3-Humidity(RAW)"<<"\t"<<"3-Humidity(ENG)" <<
                          "\t" << "4-Temperature(RAW)"<<"\t"<<"4-Temperature(ENG)" <<
                          "\t" << "5-Atmosferic Presure(RAW)"<<"\t"<<"5-Atmosferic Presure(ENG)" <<"\n";


                for(int runner_pluvio=0;runner_pluvio<list_pluvio[parse].count_elements();runner_pluvio++)
                {
                    stream<<list_pluvio[parse].get_item_list_time_stamp(runner_pluvio).replace(".",",")<<"\t";
                    //esta primera lista carga los valores de los sensores.

                    stream<<list_pluvio[parse].get_item_list_raw_precipitation(runner_pluvio)<<"\t";
                    stream<<list_pluvio[parse].get_item_list_precipitation(runner_pluvio).replace(".",",")<<"\t";

                    stream<<list_pluvio[parse].get_item_list_raw_acumulada(runner_pluvio)<<"\t";
                    stream<<list_pluvio[parse].get_item_list_acumulada(runner_pluvio).replace(".",",")<<"\t";

                    stream<<list_pluvio[parse].get_item_list_raw_humidity(runner_pluvio)<<"\t";
                    stream<<list_pluvio[parse].get_item_list_humidity(runner_pluvio).replace(".",",")<<"\t";

                    stream<<list_pluvio[parse].get_item_list_raw_temperature(runner_pluvio)<<"\t";
                    stream<<list_pluvio[parse].get_item_list_temperature(runner_pluvio).replace(".",",")<<"\t";

                    stream<<list_pluvio[parse].get_item_list_raw_presure(runner_pluvio)<<"\t";
                    stream<<list_pluvio[parse].get_item_list_presure(runner_pluvio).replace(".",",")<<"\t";

                    stream<<"\n";
                }
            }

            pluvio_file.close();

        }

    }

    //***********************************************************************************************
    //**********************************PARSEO DE DATOS NIVELES**************************************
    //***********************************************************************************************
    for (int parse=0;parse<list_niveles.count();parse++) {

        if(check_niveles[parse]->isChecked()){

            QFile nivel_file(direction_save+"/"+list_niveles[parse].get_id_device()+".csv");
            if(nivel_file.open(QIODevice::ReadWrite))
            {
                QTextStream stream( &nivel_file);
                stream << "time_stamp"<< "\t" << "1-Temperature(RAW)"<<"\t"<<"1-Temperature(ENG)"<< "\t"<<
                          "2-Nivel(RAW)"<<"\t"<<"2-Nivel(ENG)" <<
                          "\t" << "3-Threshold Vibration(RAW)"<<"\t"<<"3-Threshold Vibration(ENG)" <<
                          "\t" << "4-Threshold Sound(RAW)"<<"\t"<<"4-Threshold Sound(ENG)" <<"\n";


                for(int runner_nivel=0;runner_nivel<list_niveles[parse].count_elements();runner_nivel++)
                {
                    stream<<list_niveles[parse].get_item_list_time_stamp(runner_nivel).replace(".",",")<<"\t";
                    //esta primera lista carga los valores de los sensores.

                    stream<<list_niveles[parse].get_item_list_raw_temperature(runner_nivel)<<"\t";
                    stream<<list_niveles[parse].get_item_list_temperature(runner_nivel).replace(".",",")<<"\t";

                    stream<<list_niveles[parse].get_item_list_raw_nivel(runner_nivel)<<"\t";
                    stream<<list_niveles[parse].get_item_list_nivel(runner_nivel).replace(".",",")<<"\t";

                    stream<<list_niveles[parse].get_item_list_raw_vibration(runner_nivel)<<"\t";
                    stream<<list_niveles[parse].get_item_list_vibration(runner_nivel).replace(".",",")<<"\t";

                    stream<<list_niveles[parse].get_item_list_raw_sound(runner_nivel)<<"\t";
                    stream<<list_niveles[parse].get_item_list_sound(runner_nivel).replace(".",",")<<"\t";

                    stream<<"\n";
                }
            }
            nivel_file.close();
        }
    }

    //***********************************************************************************************
    //**********************************PARSEO DE DATOS UMBRALES*************************************
    //***********************************************************************************************
    for (int parse=0;parse<list_umbrales.count();parse++) {
        if(check_umbrales[parse]->isChecked()){
            QFile umbral_file(direction_save+"/"+list_umbrales[parse].get_id_device()+".csv");
            if(umbral_file.open(QIODevice::ReadWrite))
            {
                QTextStream stream( &umbral_file);
                stream << "time_stamp"<< "\t" << "1-Acc-X(RAW)"<<"\t"<<"1-Acc-X(ENG)"<< "\t"<< "2-Acc-Y(RAW)"<<"\t"<<"2-Acc-Y(ENG)" << "\t"<< "3-Acc-Z(RAW)"<<"\t"<<"3-Acc-Z(ENG)";
                stream <<"\t" << "4-Hum(RAW)"<<"\t"<<"4-Hum(ENG)"<< "\t";
                stream <<"5-Vib-X(RAW)"<<"\t"<<"5-Vib-X(ENG)"<< "\t"<< "6-Vib-Y(RAW)"<<"\t"<<"6-Vib-Y(ENG)"<<"\t"<<"7-Vib-Z(RAW)"<<"\t"<<"7-Vib-Z(ENG)"<<"\n";

                for(int runner_umbral=0;runner_umbral<list_umbrales[parse].count_elements();runner_umbral++)
                {
                    stream<<list_umbrales[parse].get_item_list_time_stamp(runner_umbral).replace(".",",")<<"\t";
                    //esta primera lista carga los valores de acceleracion raw y eng

                    stream<<list_umbrales[parse].get_item_list_raw_acc_x(runner_umbral)<<"\t";
                    stream<<list_umbrales[parse].get_item_list_acc_x(runner_umbral).replace(".",",")<<"\t";


                    stream<<list_umbrales[parse].get_item_list_raw_acc_y(runner_umbral)<<"\t";
                    stream<<list_umbrales[parse].get_item_list_acc_y(runner_umbral).replace(".",",")<<"\t";

                    stream<<list_umbrales[parse].get_item_list_raw_acc_z(runner_umbral)<<"\t";
                    stream<<list_umbrales[parse].get_item_list_acc_z(runner_umbral).replace(".",",")<<"\t";
                    //esta carga los valores de humedad
                    stream<<list_umbrales[parse].get_item_list_raw_hum(runner_umbral)<<"\t";
                    stream<<list_umbrales[parse].get_item_list_hum(runner_umbral).replace(".",",")<<"\t";
                    //esta parte carga los valores de vibrcion

                    stream<<list_umbrales[parse].get_item_list_raw_vib_x(runner_umbral)<<"\t";
                    stream<<list_umbrales[parse].get_item_list_vib_x(runner_umbral).replace(".",",")<<"\t";


                    stream<<list_umbrales[parse].get_item_list_raw_vib_y(runner_umbral)<<"\t";
                    stream<<list_umbrales[parse].get_item_list_vib_y(runner_umbral).replace(".",",")<<"\t";

                    stream<<list_umbrales[parse].get_item_list_raw_vib_z(runner_umbral)<<"\t";
                    stream<<list_umbrales[parse].get_item_list_vib_z(runner_umbral).replace(".",",");

                    stream<<"\n";
                }
            }

            umbral_file.close();
        }
    }

    //***********************************************************************************************
    //**********************************PARSEO DE DATOS DISTANCIOMETRO*******************************
    //***********************************************************************************************
    for (int parse=0;parse<list_distan.count();parse++) {

        if(check_distanciometer[parse]->isChecked()){

            QFile distan_file(direction_save+"/"+list_distan[parse].get_id_device()+".csv");
            if(distan_file.open(QIODevice::ReadWrite))
            {
                QTextStream stream( &distan_file);
                stream << "time_stamp"<< "\t" << "1-Distanciometer(RAW)"<<"\t"<<"1-Distanciometer(ENG)""\n";


                for(int runner_distan=0;runner_distan<list_distan[parse].count_elements();runner_distan++)
                {
                    stream<<list_distan[parse].get_item_list_time_stamp(runner_distan).replace(".",",")<<"\t";
                    //esta primera lista carga los valores de los sensores.

                    stream<<list_distan[parse].get_item_list_raw_distanciometer(runner_distan)<<"\t";
                    stream<<list_distan[parse].get_item_list_distanciometer(runner_distan).replace(".",",")<<"\t";


                    stream<<"\n";
                }
            }
            distan_file.close();
        }
    }

    //***********************************************************************************************
    //**********************************PARSEO DE DATOS FERMENTADOR**********************************
    //***********************************************************************************************
    for (int parse=0;parse<list_fermenter.count();parse++) {

        if(check_fermenter[parse]->isChecked()){

            QFile ferme_file(direction_save+"/"+list_fermenter[parse].get_id_device()+".csv");
            if(ferme_file.open(QIODevice::ReadWrite))
            {
                QTextStream stream( &ferme_file);
                stream << "time_stamp"<< "\t" << "1-PH(RAW)"<<"\t"<<"1-PH(ENG)"<< "\t"
                       << "2-Resistivity-1(RAW)"<<"\t"<<"2-Resistivity-1(ENG)" << "\t"
                       << "3-Resistivity-2(RAW)"<<"\t"<<"3-Resistivity-2(ENG)" <<"\t"
                       << "4-Temperature-1(RAW)"<<"\t"<<"4-Temperature-1(ENG)"<< "\t"
                       <<"5-Temperature-2(RAW)"<<"\t"<<"5-Temperature-2(ENG)"<< "\t"
                       << "6-Temperature-3(RAW)"<<"\t"<<"6-Temperature-3(ENG)"<<"\t"
                       <<"7-Temperature-4(RAW)"<<"\t"<<"7-Temperature-4(ENG)"<<"\t"
                       <<"8-Alcohol(RAW)"<<"\t"<<"8-Alcohol(ENG)"<<"\t"
                       <<"9-Methane(RAW)"<<"\t"<<"9-Methane(ENG)"<<"\n";

                for(int runner_distan=0;runner_distan<list_fermenter[parse].count_elements();runner_distan++)
                {
                    stream<<list_fermenter[parse].get_item_list_time_stamp(runner_distan).replace(".",",")<<"\t";
                    //esta primera lista carga los valores de los sensores.

                    stream<<list_fermenter[parse].get_item_list_raw_ph(runner_distan)<<"\t";
                    stream<<list_fermenter[parse].get_item_list_ph(runner_distan).replace(".",",")<<"\t";

                    stream<<list_fermenter[parse].get_item_list_raw_resistivity1(runner_distan)<<"\t";
                    stream<<list_fermenter[parse].get_item_list_resistivity1(runner_distan).replace(".",",")<<"\t";

                    stream<<list_fermenter[parse].get_item_list_raw_resistivity2(runner_distan)<<"\t";
                    stream<<list_fermenter[parse].get_item_list_resistivity2(runner_distan).replace(".",",")<<"\t";

                    stream<<list_fermenter[parse].get_item_list_raw_temperature1(runner_distan)<<"\t";
                    stream<<list_fermenter[parse].get_item_list_temperature1(runner_distan).replace(".",",")<<"\t";

                    stream<<list_fermenter[parse].get_item_list_raw_temperature2(runner_distan)<<"\t";
                    stream<<list_fermenter[parse].get_item_list_temperature2(runner_distan).replace(".",",")<<"\t";

                    stream<<list_fermenter[parse].get_item_list_raw_temperature3(runner_distan)<<"\t";
                    stream<<list_fermenter[parse].get_item_list_temperature3(runner_distan).replace(".",",")<<"\t";

                    stream<<list_fermenter[parse].get_item_list_raw_temperature4(runner_distan)<<"\t";
                    stream<<list_fermenter[parse].get_item_list_temperature4(runner_distan).replace(".",",")<<"\t";

                    stream<<list_fermenter[parse].get_item_list_raw_alcohol(runner_distan)<<"\t";
                    stream<<list_fermenter[parse].get_item_list_alcohol(runner_distan).replace(".",",")<<"\t";

                    stream<<list_fermenter[parse].get_item_list_raw_methano(runner_distan)<<"\t";
                    stream<<list_fermenter[parse].get_item_list_methano(runner_distan).replace(".",",")<<"\t";


                    stream<<"\n";
                }
            }
            ferme_file.close();
        }
    }


    ui->console->append("Parsed data successfully!");
}

void translator::change_estate_dir(const QString data)
{
    //qDebug()<<"change value"<<data;
    //primero obtenemos la direccion actua que se tiene almacenada
    QString direccion= ui->db_direction->text();
    if(direccion.contains(".db"))
    {
        //si sucede esto contiene una base de datos.
        ui->btn_test_db->setEnabled(true);
        ui->date1->setEnabled(true);
        ui->date2->setEnabled(true);
    }
    else {
        ui->btn_test_db->setEnabled(false);
        ui->date1->setEnabled(false);
        ui->date2->setEnabled(false);
    }
}

void translator::dateStart(QString dateStart){
    date1 = dateStart;
    //    qDebug()<<"STARTTTT " +date1;
}

void translator::dateEnd(QString dateEnd){
    date2 = dateEnd;
    //    qDebug()<<"ENDDDDDDD " + date2;
}

void translator::dateStartSlot(QDate date){
    //Se le pasa a la fecha inicial
    QString timeStart = "00:00:00";
    QString parseStart = QDate(date).toString("yyyy/MM/dd")+ " " +timeStart;
    //    if(getDateStart() < getDateEnd()){
    //        ui->msgError->setText("La fecha inicial es menor que la final");
    //    }else{
    //        ui->msgError->setText("");
    //    }
    return dateStart(parseStart);
    //    qDebug()<<parseStart;
}

void translator::dateEndSlot(QDate date){
    //Se le pasa a la fecha final
    QString timeEnd = "23:59:59";
    QString parseEnd = QDate(date).toString("yyyy/MM/dd")+ " " +timeEnd ;

    return dateEnd(parseEnd);
    //    qDebug()<<parseEnd;
}

QString translator::getDateStart(){
    return date1;
}

QString translator::getDateEnd(){
    return date2;
}
