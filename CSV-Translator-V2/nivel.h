#ifndef NIVEL_H
#define NIVEL_H

#include <QObject>

class nivel
{
public:
    bool status=false;
    nivel();

    void setID(int id);
    int getID();

    //setters anf getters id_device
    void set_id_device(QString my_id_device);
    QString get_id_device();
    //setters and getters time_stamp
    void add_item_time(QString add_time);
    QStringList get_list_time_stamp();
    QString get_item_list_time_stamp(int position);
    //*********************************************************************
    //setters and getters nivel
    void add_item_nivel(QString nivel);
    QStringList get_list_nivel();
    QString get_item_list_nivel(int position);

    void add_item_raw_nivel(QString nivel);
    QStringList get_list_raw_nivel();
    QString get_item_list_raw_nivel(int position);

    //*********************************************************************
    //setters and getters temperatura
    void add_item_temperature(QString temperature);
    QStringList get_list_temperature();
    QString get_item_list_temperature(int position);

    void add_item_raw_temperature(QString temperature);
    QStringList get_list_raw_temperature();
    QString get_item_list_raw_temperature(int position);
    //*********************************************************************

    //*********************************************************************
    //setters and getters vibration
    void add_item_vibration(QString vibration);
    QStringList get_list_vibration();
    QString get_item_list_vibration(int position);

    void add_item_raw_vibration(QString vibration);
    QStringList get_list_raw_vibration();
    QString get_item_list_raw_vibration(int position);
    //*********************************************************************

    //*********************************************************************
    //setters and getters temperatura
    void add_item_sound(QString sound);
    QStringList get_list_sound();
    QString get_item_list_sound(int position);

    void add_item_raw_sound(QString sound);
    QStringList get_list_raw_sound();
    QString get_item_list_raw_sound(int position);
    //*********************************************************************


    int count_elements();

private:
    int ID;
    QString id_device;
    QStringList list_time_stamp;
    QStringList list_nivel;
    QStringList list_temperatura;
    QStringList list_vibration;
    QStringList list_sound;

    QStringList list_Raw_nivel;
    QStringList list_Raw_temperatura;
    QStringList list_Raw_vibration;
    QStringList list_Raw_sound;
};

#endif // NIVEL_H
