#ifndef DISTANCIOMETER_H
#define DISTANCIOMETER_H

#include <QObject>

class distanciometer
{
public:
    distanciometer();

    void setID(int id);
    int getID();

    //setters anf getters id_device
    void set_id_device(QString my_id_device);
    QString get_id_device();
    //setters and getters time_stamp
    void add_item_time(QString add_time);
    QStringList get_list_time_stamp();
    QString get_item_list_time_stamp(int position);

    //setters and getters distanciometer
    void add_item_distanciometer(QString distan);
    QStringList get_list_distanciometer();
    QString get_item_list_distanciometer(int position);

    void add_item_raw_distanciometer(QString distan);
    QStringList get_list_raw_distanciometer();
    QString get_item_list_raw_distanciometer(int position);

    int count_elements();

private:
    int ID;
    QString id_device;
    QStringList list_time_stamp;
    QStringList list_distanciometer;

    QStringList list_Raw_distanciometer;
};

#endif // DISTANCIOMETER_H
