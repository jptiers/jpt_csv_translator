#ifndef JPTDATABASE_H
#define JPTDATABASE_H

#include <QObject>
#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlQuery>
#include <QtSql/QSqlError>
#include <QString>
#include <QStringList>
#include <QVector>
#include <QVariant>
#include "../model_nodex.h"


//Nota: Cuando SQLite te devulve el vector de respuesta luego de reliazar una consult,
//este vector tienes las columnas en el orden en el que se encuentran en la base de datos
//Ejmeplo: Tabale: persona; Contienelas columnas : Nombre, apellido, feccha_nacimiento
//cuando se obtiene la respuesta de la consuta a la base de datos, entonces se tiene un elemento o elementos de tipo peersona.
//por ende, para consultar el nombre se haria :
//si la respuesta es solo un elemento-> elemento_persona[0]= nombre de la persona , elemento_persona[1]
//= apellido de la persona...
//Si se definen variables: ejemplo, #define def_pos_nombre 0 => se haria la consuta asi: elemento_persona[def_pos_nombe]

#define def_pos_table_func  0
#define def_pos_table_conf  1
#define def_pos_table_char  2
#define def_pos_table_modu  3
#define def_pos_table_pwms  4
#define def_pos_table_dela  5

#define def_num_colums_table_func 10
#define def_num_colums_table_conf 25
#define def_num_colums_table_char 23 // +2 fact (Ya sumados)
#define def_num_colums_table_modu 2
#define def_num_colums_table_pwms 9
#define def_num_colums_table_dela 4

class jptdatabase : public QObject{
    Q_OBJECT

public:

    explicit jptdatabase(QObject *parent = nullptr);
    ~jptdatabase();

    void set_db_name();
    void update_register(QString name_table, QString table_values);
    void update_register(QString name_table, QString id, QString query);
    void get_values(int pos_name_table, QVector<QStringList > *vec_data);
    void init_database_connection(QString name_data_base);


    void update_port_cloud(QString port_cloud);
    void update_ip_cloud(QString ip_cloud);
    void update_hub_name(QString hub_name);

    void charge_data_on_memory(QString startDate, QString endDate);
    void get_all_data(QString startDate, QString endDate);

    //getters de continuidad
    QList<model_nodex> getDataAllNodex();

public slots:
    void insert_register (QString name_table, QString table_values);
    void update_level();
    QStringList get_data_config();
    void set_data_config(int id,QString dato);

//Los procesos reciben informacion por medio de variables y realizan un procedimiento con esas variables.
//los slots, reciben señales, alamacenan informacion y realizan procesos
    //por ende para manejar un slot, es necesario manejarlo por medio de sistma connect.
    //los slots pueden manejar diferentes procesos internos en ellos.


signals:
#ifdef _COMPILE_MESSAGE
    void viewer_status(QString status);
#endif

private:
    QList<model_nodex> data_nodex;
    QSqlDatabase db;
};

#endif // JPTDATABASE_H
