#include "jptdatabase.h"
#include <QtDebug>
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>
#include "../translator.h"
const QString           gv_list_name_tables[6] = {"Functions","Configurations","Characteristics","Modules","PWM","Delayer"};


//*********************************************************************************************************************************************************
// Constructor
//*********************************************************************************************************************************************************
jptdatabase::jptdatabase(QObject *parent) : QObject(parent){

}
//*********************************************************************************************************************************************************
// Inicializa la conexion con la base de datos
//*********************************************************************************************************************************************************

void jptdatabase::init_database_connection(QString name_data_base){
    //This process only seup he database system.

    db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName(name_data_base);
    //db.setPassword(password);//variable requerida solo si la base de datos lo amerita
    //db.setUserName(user_name);//variable requerida si la base de datos lo amerita.
    db.open();

    /*
    if(db.open())
        emit viewer_status("[o] Database");
    else
        emit viewer_status("[*] Database");
     */

}

void jptdatabase::charge_data_on_memory(QString startDate, QString endDate)
{
    try {
        get_all_data(startDate, endDate);
    } catch (...) {
        qDebug()<<"Error al obtener toda la data";
    }

}

void jptdatabase::get_all_data(QString startDate, QString endDate){
    QString new_query;
    //VALIDA SI TIENE FECHAS

    if(startDate != "2000/01/01 00:00:00" && endDate != "2000/01/01 23:59:59"
            && startDate != "" && endDate != ""){
        new_query.append("select id, m_datetime, m_data, m_device from Nodex "
                         "WHERE m_datetime BETWEEN '"+ startDate +"' and '"+ endDate +"';");;
    }else{
        new_query.append("select id, m_datetime, m_data, m_device from Nodex;");
    }

    QSqlQuery create;
    create.prepare(new_query);
    create.exec();

    data_nodex.clear();

    while(create.next()){
        QString id = create.value(0).toString();
        QString time = create.value(1).toString();
        QString data = create.value(2).toString();
        QString device = create.value(4).toString();

        int parseID = id.toInt();

        model_nodex temp_model;
        temp_model.init_model(parseID,data,device,time);
        data_nodex.append(temp_model);
    }

    qDebug()<<data_nodex.count();
}

//lo mas optimo es crear un elemento o clase denominado data
//data tendria 4 elementos los cuales seriam id, tiempo, data, device
//luego se genera solamente una lista de de datos de la forma QList<data> my_data.

QList<model_nodex> jptdatabase::getDataAllNodex(){
    return data_nodex;
}

//*********************************************************************************************************************************************************
// Destructor
//*********************************************************************************************************************************************************
jptdatabase::~jptdatabase(){
    db.close();
}
//*********************************************************************************************************************************************************
// Insertar Datos en las tablas
//*********************************************************************************************************************************************************
void jptdatabase::insert_register(QString name_table, QString query){
    QString new_query;
    new_query.append("INSERT INTO " + name_table + query);
    qDebug()<<"QUERY TO SQLITE";
    qDebug()<<new_query;
    QSqlQuery create;
    create.prepare(new_query);

    create.exec();
}
//*********************************************************************************************************************************************************
// Obtener los registros necesitados
//*********************************************************************************************************************************************************
void jptdatabase::get_values(int pos_name_table, QVector<QStringList > *vec_data){
    vec_data->clear();

    QString lv_name_table(gv_list_name_tables[pos_name_table]);
    int lv_num_item_table(0);

    if(pos_name_table == def_pos_table_func)
        lv_num_item_table  = def_num_colums_table_func;
    else if(pos_name_table == def_pos_table_conf)
        lv_num_item_table  = def_num_colums_table_conf;
    else if(pos_name_table == def_pos_table_char)
        lv_num_item_table  = def_num_colums_table_char;
    else if(pos_name_table == def_pos_table_modu)
        lv_num_item_table  = def_num_colums_table_modu;
    else if(pos_name_table == def_pos_table_pwms)
        lv_num_item_table  = def_num_colums_table_pwms;
    else if(pos_name_table == def_pos_table_dela)
        lv_num_item_table  = def_num_colums_table_dela;

    QString lv_query;
    lv_query.append("SELECT * FROM " + lv_name_table + " ORDER BY id ");
    QSqlQuery lv_get_query;
    lv_get_query.setForwardOnly(true);
    lv_get_query.prepare(lv_query);

    if(lv_get_query.exec())
        while(lv_get_query.next()){
            QStringList lv_row_data;
            for(int f_colum = 1; f_colum < lv_num_item_table; ++f_colum)
                lv_row_data << lv_get_query.value(f_colum).toString();
            vec_data->append(lv_row_data);
        }

}
//*********************************************************************************************************************************************************
// Actualizar los registros de la base de datos
//*********************************************************************************************************************************************************
void jptdatabase::update_register(QString name_table, QString query){
    QString lv_query;
    lv_query.append("UPDATE " + name_table + " SET " + query + ";");
    QSqlQuery lv_get_query;
    lv_get_query.setForwardOnly(true);
    lv_get_query.prepare(lv_query);

    lv_get_query.exec();

}
//*********************************************************************************************************************************************************
// Actualizar los registros de la base de datos
//*********************************************************************************************************************************************************
void jptdatabase::update_register(QString name_table, QString id, QString query){
    QString lv_query;
    lv_query.append("UPDATE " + name_table + " SET " + query + " WHERE id=" + id + ";");
    QSqlQuery lv_get_query;
    lv_get_query.setForwardOnly(true);
    lv_get_query.prepare(lv_query);

    lv_get_query.exec();
}

void jptdatabase::update_hub_name(QString hub_name)
{
    QString lv_query;

    lv_query="UPDATE Configurations SET hub_name='"+hub_name+"'  WHERE id=1";
    QSqlQuery lv_get_query;
    lv_get_query.setForwardOnly(true);
    lv_get_query.prepare(lv_query);

    if(lv_get_query.exec())
    {
        qDebug()<<"Success Query: hub_name";
    }

}

void jptdatabase::update_ip_cloud(QString ip_cloud)
{
    QString lv_query;

    lv_query="UPDATE Configurations SET ip_server='"+ip_cloud+"'  WHERE id=1";
    QSqlQuery lv_get_query;
    lv_get_query.setForwardOnly(true);
    lv_get_query.prepare(lv_query);

    if(lv_get_query.exec())
    {
        qDebug()<<"Success Query: ip_cloud";
    }

}

void jptdatabase::update_port_cloud(QString port_cloud)
{
    QString lv_query;

    lv_query="UPDATE Configurations SET port_server='"+port_cloud+"'  WHERE id=1";
    QSqlQuery lv_get_query;
    lv_get_query.setForwardOnly(true);
    lv_get_query.prepare(lv_query);

    if(lv_get_query.exec())
    {
        qDebug()<<"Success Query: port_cloud";
    }

}

void jptdatabase::update_level()
{

    QString lv_query;
    lv_query.append("INSERT INTO nivel_1(time_stamp,nivel,temperature) VALUES('2019-05-27 10:00:01',7,7);");
    QSqlQuery lv_get_query;
    lv_get_query.setForwardOnly(true);
    lv_get_query.prepare(lv_query);
    lv_get_query.exec();

}

QStringList jptdatabase::get_data_config()
{
    QString lv_query;
    lv_query.append("SELECT device FROM config_charts order by id;");
    QSqlQuery lv_get_query;
    lv_get_query.setForwardOnly(true);
    lv_get_query.prepare(lv_query);
    QStringList data_to_send;

    if(lv_get_query.exec())
    {
        while(lv_get_query.next()){
            data_to_send<< lv_get_query.value(0).toString();
            //qDebug()<<lv_get_query.value(0).toString();

        }
    }
    return data_to_send;


}

void jptdatabase::set_data_config(int id,QString dato)
{
    QString lv_query;

    lv_query="UPDATE config_charts SET device='"+dato+"'  WHERE id="+QString::number(id)+";";
    QSqlQuery lv_get_query;
    lv_get_query.setForwardOnly(true);
    lv_get_query.prepare(lv_query);

    if(lv_get_query.exec())
    {
        qDebug()<<"Success Query: update device"<<id<<"//"<<dato;
    }
}
