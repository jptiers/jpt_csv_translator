# CSV_SAN_EDUARDO.
## Parseador de datos, transforma los datos a archivos con extension .csv para el respectivo analisis de las transmisiones de los nodos en San Eduardo, Boyaca, Colombia.
 
El software en mension, esta escrito bajo el lenguaje de programacion C++, construido y compilado bajo el framework Qt (basado en C++), framework que permite la construccion de software con interfaz grafica de bajo nivel y su funcionamiento para poco recursos en cuestion de computo.

**_Version del framework Qt 5.9.1._**

La funcionalidad del software desde su primera version se utiliza en el sistema operativo linux, en este respositorio cuenta con el codigo fuente y ademas de su compilacion para ejecucion en linux.

**Recomendaciones al usar el software:**

-Al momento de importar la direccion de la base de datos donde este ubicada, la carpeta o la direccion donde se encuentre no debe de contener espacios en la direccion, ejemplo:

`home/descargas/base_de_datos/jptdatabase_17_oct.db` <- **Esta es la forma correcta**.

`home/descargas/base de datos/jptdatabase 17 oct.db` <- **Forma incorrecta de la ruta**.

-El sistema solo reconoce archivo con extension '**.db**'.

-La fecha incial de consulta no debe superar la final y la fecha final no debe de ser menor que la inicial.

-**El formato de la fecha esta en funcion de dd/mm/aa**.

-**Cuando liste los sensores que estan almacenados deben ser seleccionados (checkeados) para su respectivo parseo**.

-**Los archivos que se generan tienen como nombre el identificador del dispositivo con su respectiva extension .csv, y se almacenan en la ruta donde esta ubicada la base datos seleccionada**.

-**El sistema cuenta con una pequeña consola que informa si la base de datos fue cargada, cuenta la cantidad de sensores y notifica cuando esta listo el parseo de los datos**.

Teniendo en cuenta estos parametros el sistema debe funcionar correctamente.

Al iniciar el software tenemos una interfaz la cual es muy sencilla de usar, solo se necesita seleccionar la ubicacion de la **base de datos** (teniendo en cuenta las recomendaciones anteriores). 
Despues de eso se habilita el boton para validar inicialmente el parseo de la base de datos, teniendo en cuenta que primero se debe seleccionar las fechas en la que deseamos parsear nuestros datos; se presiona el boton de parseo, valida que sensores estan activos y asi mismo los lista. Acto seguido, seleccionamos que sensores deseamos parsear y solo resta transformar la data.
La consola notifica y revisamos en la ruta donde esta la base de datos para verificar los archivos.
